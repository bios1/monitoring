import pandas as pd
from monitoring_tool import util
from monitoring_tool import constants

class Alert:
    #class attributes
    maxStatus = 3 # 0=unchecked 1=acknwoledged 2=fixed 3=ignored
    maxLevel = 3 # 0=non-important 3=critical
    maxLenComment = 255 #max number of characters in a comment
    
    #constructor
    def __init__(self, database, typeAlert, levelAlert, sensor_instance_id, comment="", periodStart=None, periodEnd=None):
        self.database = database
        self.type = typeAlert
        self.level = util.bounds(0, levelAlert, Alert.maxLevel)
        self.sensor_instance_id = sensor_instance_id
        self.comment = comment
        constants.setTimezones(True)
        self.dateAlert = util.getLocalTime(constants.glob_str_tz_exec, constants.glob_str_tz_dest)
        self.status = 0
        self.userID = None
        self.periodStart = periodStart
        self.periodEnd = periodEnd
        self.isRedundant = False
        
    def getSensorInstanceForeignKey(self):
        query = ("SELECT {pk} FROM {t}"
                " WHERE {t}.{c1} = %s")
        query = query.format(t = self.database.properties["sensorInstance_table"],
                            pk = self.database.properties["sensorInstance_pk"],
                            c1 = self.database.properties["sensorInstance_id"] )
        param = (self.sensor_instance_id, )
        if(self.database.properties["version_sensitive"] == '1'):
            version = self.database.version
            query += " AND {t}.{c2} = %s"
            param = (self.sensor_instance_id, version)
            query = query.format(t = self.database.properties["sensorInstance_table"],
                                c2 = self.database.properties["sensorInstance_version"])
        
        temp = pd.read_sql_query(query, self.database.conn, params=param)
        temp = temp.to_numpy()
        foreignKey = temp[0][0].item()
        return foreignKey
    
    def checkRedundancyDB(self):
        fk = self.getSensorInstanceForeignKey()
        fmt = '%Y-%m-%d %H:%M:%S'
        query = ("SELECT {pk} FROM {t}"
                " WHERE {c1} = %s"
                " AND {c2} = %s"
                " AND {c3} = %s")
        query = query.format(t = self.database.properties["alerts_table"],
                            pk = self.database.properties["alerts_pk"],
                            c1 = self.database.properties["alerts_fk_sensorInstance"],
                            c2 = self.database.properties["alerts_type"],
                            c3 = self.database.properties["alerts_status"])
        param = (fk, self.type, 0)
        if(self.periodStart is not None): #check is not None to prevent SQL comparison to NULL
            str_periodStart = self.periodStart.strftime(fmt)
            query += " AND {c4} <= %s " #consider the new alert redundant if an existing alert covers previous data 
            query = query.format(c4 = self.database.properties["alerts_periodstart"])
            param += (str_periodStart, )
        if(self.periodEnd is not None): #check is not None to prevent SQL comparison to NULL
            str_periodEnd = self.periodEnd.strftime(fmt)
            query += " AND {c5} >= %s " #consider the new alert is redundant if an existing alert covers data further
            query = query.format(c5 = self.database.properties["alerts_periodend"])
            param += (str_periodEnd, )
        temp = pd.read_sql_query(query, self.database.conn, params=param)
        #if self.periodStart is not None and self.periodEnd is not None, consider the new alert is redundant if an existing alert contains the time span covered by the new alert
        if(len(temp.index) >= 1): #there is at least 1 similar unsolved problem on the same sensor (normally at most 1 similar alert)
            self.isRedundant = True
            temp = temp.to_numpy()
            targetID = temp[0][0].item()
            return targetID #the alert is redundant, returns the ID of the similar alert
        else:
            self.isRedundant = False
            return -1 #the alert is new, returns -1 since it is not a possible ID
    
    def insertDB(self):
        #timestamps to string conversion to fit in SQL queries
        fmt = '%Y-%m-%d %H:%M:%S'
        str_date = self.dateAlert.strftime(fmt)
        
        targetID = self.checkRedundancyDB()
        if(targetID == -1): #if the alert is new, insert it
            fk = self.getSensorInstanceForeignKey()
            query = ("INSERT INTO {t} "
                    " ({pk}, {c1}, {c2}, {c3}, {c4}, {c5}, {c6}, {c7}, {c8}, {c9}) "
                    "VALUES(DEFAULT, %s, %s, %s, %s, %s, %s, %s, %s, %s) ;" )
            query = query.format(t = self.database.properties["alerts_table"],
                                pk = self.database.properties["alerts_pk"],
                                c1 = self.database.properties["alerts_comment"],
                                c2 = self.database.properties["alerts_timestamp"],
                                c3 = self.database.properties["alerts_status"],
                                c4 = self.database.properties["alerts_type"],
                                c5 = self.database.properties["alerts_user"],
                                c6 = self.database.properties["alerts_level"],
                                c7 = self.database.properties["alerts_fk_sensorInstance"],
                                c8 = self.database.properties["alerts_periodstart"],
                                c9 = self.database.properties["alerts_periodend"])
            param = (self.comment, str_date, self.status,
                    self.type, self.userID, self.level,
                    fk) #parameters in the order that corresponds to the columns above
            if(self.periodStart is not None):
                str_periodStart = self.periodStart.strftime(fmt)
                param += (str_periodStart, )
            else:
                param += (self.periodStart, )
            if(self.periodEnd is not None):
                str_periodEnd = self.periodEnd.strftime(fmt)
                param += (str_periodEnd, )
            else:
                param += (self.periodEnd, )
            cursor = self.database.conn.cursor()
            cursor.execute(query, param)
            self.database.conn.commit()
            return True
        else: #if the alert is redundant, update the old alert
            #update the comment
            query = "UPDATE {t} SET {c1} = %s WHERE {pk} = %s ;"
            query = query.format(t = self.database.properties["alerts_table"],
                                pk = self.database.properties["alerts_pk"],
                                c1 = self.database.properties["alerts_comment"])
            param = (self.comment, targetID)
            cursor = self.database.conn.cursor()
            cursor.execute(query, param)
            self.database.conn.commit()
            #update timestamp
            query = "UPDATE {t} SET {c2} = %s WHERE {pk} = %s ;"
            query = query.format(t = self.database.properties["alerts_table"],
                                pk = self.database.properties["alerts_pk"],
                                c2 = self.database.properties["alerts_timestamp"])
            param = (str_date, targetID)
            cursor = self.database.conn.cursor()
            cursor.execute(query, param)
            self.database.conn.commit()
            #update level
            query = "UPDATE {t} SET {c3} = %s WHERE {pk} = %s ;"
            query = query.format(t = self.database.properties["alerts_table"],
                                pk = self.database.properties["alerts_pk"],
                                c3 = self.database.properties["alerts_level"])
            param = (self.level, targetID)
            cursor = self.database.conn.cursor()
            cursor.execute(query, param)
            self.database.conn.commit()
            return False
        
    def display(self):
        print("Alert for sensor_instance_id ", self.sensor_instance_id, " of type ", self.type, " and level ", self.level, " raised at ", self.dateAlert, " current status: ", self.status)
        print("Comment: ", self.comment)
        print("\n")
