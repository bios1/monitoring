import pandas as pd

def read_addresses_in_DB(database):
    """
    read_addresses_in_DB
    
    Args:
        database: Database object representing the target database
        
    Returns:
        l_addresses: a list of lists of strings representing email addresses and names, l_addresses[i] = [address, name]
    """
    #creates the query
    query = ("SELECT {c1}, {c2} "
            " FROM {t} ")
    query = query.format(t = database.properties["emailAddresses_table"],
                        c1 = database.properties["emailAddresses_address"],
                        c2 = database.properties["emailAddresses_name"] )
    
    #manipulates the resulting dataframe
    df = pd.read_sql_query(query, database.conn)
    l_addresses = df.values.tolist()
    
    return l_addresses