#exple : python3 receive_logs_email.py

#!/usr/bin/env python
import pika, sys, smtplib, ssl, getpass, os
from socket import gaierror
from monitoring_tool.database import Database
from util.database_emails import read_addresses_in_DB

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage : python receive_logs_email.py <database_name>")
        exit()

    db_file = "{}/.config/monitoring_tool/".format(os.environ['HOME']) + sys.argv[1] + ".txt"
    database = Database(db_file)
    database.connect()
    tmp_emails = read_addresses_in_DB(database)
    database.disconnect()
    emails = []
    for email in tmp_emails:
        emails.append(email[0])

    email_port = 465  # For SSL

    try:
        email_file = open("{}/.config/monitoring_tool/email_sender.credentials".format(os.environ['HOME']))
    except Exception as err:
        print(err)
        exit()
    
    sender_email = email_file.readline()
    sender_password = email_file.readline()
    email_file.close()

    try:
        file = open("{}/.config/monitoring_tool/rabbitmq.credentials".format(os.environ['HOME']))
    except Exception as err:
        print(err)
        exit()

    (hostname, port) = tuple(file.readline().split(':', 2))
    username = file.readline().strip()
    password = file.readline().strip()
    file.close()

    credentials = pika.PlainCredentials(username, password)
    
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=hostname, port=port, credentials=credentials))
    channel = connection.channel()
    
    exchange_name = database.properties["database"] + "-check-alert"
    channel.exchange_declare(exchange=exchange_name, exchange_type='topic')
    
    queue_name = exchange_name + "-urgent"
    result = channel.queue_declare(queue_name, durable=True)
    queue_name = result.method.queue
    
    channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key='#.3')
    
    context = ssl.create_default_context() # Create a secure SSL context
    
    print(' [*] Waiting for logs. To exit press CTRL+C')
    
    def callback(ch, method, properties, body):
    
        print(" [x] %r:%r" % (method.routing_key, body))
    
        try:
            with smtplib.SMTP_SSL("smtp.gmail.com", email_port, context=context) as server:
                server.login(sender_email, sender_password)
                server.sendmail(sender_email, emails,"Subject: Error in the database\n\n %r:%r" % (method.routing_key, body)) #send email
                print("Message sent!")
                ch.basic_ack(delivery_tag=method.delivery_tag)
    
        #possible mistakes when connecting to the email server
        except (gaierror, ConnectionRefusedError):
            print('Failed to connect to the server. Bad connection settings?')
            sys.exit(1)
        except smtplib.SMTPAuthenticationError:
            print('Failed to connect to the server. Wrong username/password?')
            sys.exit(1)
    
    channel.basic_consume(queue=queue_name, on_message_callback=callback)
    
    channel.start_consuming()