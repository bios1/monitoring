#exple : python3 receive_logs_email_period.py

#!/usr/bin/env python
import pika, sys, smtplib, ssl, getpass, os
from socket import gaierror
from monitoring_tool.database import Database
from util.database_emails import read_addresses_in_DB

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage : python receive_logs_email_period.py <database_name>")
        exit()

    db_file = "{}/.config/monitoring_tool/".format(os.environ['HOME']) + sys.argv[1] + ".txt"
    database = Database(db_file)

    try:
        file = open("{}/.config/monitoring_tool/rabbitmq.credentials".format(os.environ['HOME']))
    except Exception as err:
        print(err)
        exit()
    
    (hostname, port) = tuple(file.readline().split(':', 2))
    username = file.readline().strip()
    password = file.readline().strip()
    file.close()

    credentials = pika.PlainCredentials(username, password)
    connection = pika.BlockingConnection(pika.ConnectionParameters(
        host=hostname, port=port, credentials=credentials))
    channel = connection.channel()
    
    exchange_name = database.properties["database"] + "-check-alert"

    channel.exchange_declare(exchange=exchange_name, exchange_type='topic') #topic_logs
    queue_name = exchange_name + "-not-urgent"
    result = channel.queue_declare(queue_name, durable=True)
    
    channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key='#.1')
    channel.queue_bind(exchange=exchange_name, queue=queue_name, routing_key='#.2')
    
    print(' [*] Waiting for logs. To exit press CTRL+C')
    
    messages = []

    method_frame = True
    while method_frame:
        (method_frame, header_frame, body) = channel.basic_get(queue_name, auto_ack=True)
        if method_frame:
            messages.append(body.decode("utf-8"))

    if len(messages) == 0:
        print("No logs")
        exit()

    database.connect()
    tmp_emails = read_addresses_in_DB(database)
    database.disconnect()
    emails = []
    for email in tmp_emails:
        emails.append(email[0])

    port = 465  # For SSL

    try:
        email_file = open("{}/.config/monitoring_tool/email_sender.credentials".format(os.environ['HOME']))
    except Exception as err:
        print(err)
        exit()

    sender_email = email_file.readline()
    password = email_file.readline()
    email_file.close()
    
    context = ssl.create_default_context() # Create a secure SSL context

    try:
        with smtplib.SMTP_SSL("smtp.gmail.com", port, context=context) as server:
            server.login(sender_email, password)
            errors="Subject: Monitoring report\n\n%s" % '\n'.join(messages) #put each message on a line of the email and skip a line between messages 
            print(errors) #print email on the terminal
            server.sendmail(sender_email, emails, errors)
            print("Message sent!")
    
    #possible mistakes when connecting to the email server
    except (gaierror, ConnectionRefusedError):
        print('Failed to connect to the server. Bad connection settings?')
    except smtplib.SMTPAuthenticationError:
        print('Failed to connect to the server. Wrong username/password?')