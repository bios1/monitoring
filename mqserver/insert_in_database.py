import pika
import sys, os
from monitoring_tool.database import Database
from util.alert import Alert

if __name__ == "__main__":
    if len(sys.argv) < 2:
        print("Usage : python insert_in_database.py <database_name>")
        exit()

    db_file = "{}/.config/monitoring_tool/".format(os.environ['HOME']) + sys.argv[1] + ".txt"
    database = Database(db_file)

    try:
        file = open("{}/.config/monitoring_tool/rabbitmq.credentials".format(os.environ['HOME']))
    except Exception as err:
        print(err)
        exit()
    
    (hostname, port) = tuple(file.readline().split(':', 2))
    username = file.readline().strip()
    password = file.readline().strip()
    file.close()

    credentials = pika.PlainCredentials(username, password)
    
    connection = pika.BlockingConnection(
        pika.ConnectionParameters(host=hostname, port=port, credentials=credentials))
    channel = connection.channel()

    exchange_name = database.properties["database"] + "-check-alert"
    
    channel.exchange_declare(exchange=exchange_name, exchange_type='topic')
    
    queue_name = exchange_name + "-insert_db"
    result = channel.queue_declare(queue_name, durable=True)
    queue_name = result.method.queue
    
    binding_key = "#"
    
    def callback(ch, method, properties, body):
        (sensor_id, alert_type, alert_level) = tuple(method.routing_key.split('.', 3))
        print("Sensor ", sensor_id, " raised an alert ", alert_type, "(severity :", alert_level, ")")
        print("Message :", str(body))
        database.connect()
        al = Alert(database, int(alert_type), int(alert_level), int(sensor_id), body)
        al.insertDB()
        database.disconnect()
    
    channel.queue_bind(
        exchange=exchange_name,
        queue=queue_name,
        routing_key=binding_key)
    
    channel.basic_consume(
        queue=queue_name, on_message_callback=callback, auto_ack=True)
    
    print("Starting to listen to message...")
    channel.start_consuming()