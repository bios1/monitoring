# Nimbus research center internship: Monitoring and diagnostics tool for IoT Platforms

The Nimbus monitoring tool is a monitoring and diagnostics tool for IoT platforms.
The goal of this project is to provide a generic software which can work on 
different IoT databases. It provides checks to run on IoT data and raises
alerts when a problem is detected (missing value, abnormal value...). The alerts
are sent to RabbitMQ queues and can be handled in many ways. Some RabbitMQ
consummers are provided in this repository but you can write your own easily.

The monitoring software is easily extensible. The checks provided are general
and intended to work on every database but application-specific checks can also
be written in Python.

## Project architecture

There are two packages provided in this repository :
+ The `monitoring_tool` package which contains the monitoring application and
the checks.
+ The `mqserver` package which contains RabbitMQ consummers.

## Setup

### Requirements

The monitoring tool works on MySQL databases which follow a simple architecture:
+ There must be one table for the sensors data with:
  + A column for the value of the measure
  + A column for the ID of the sensor which made the measure
  + A timestamp column for the date of the measure (optional)
+ There must be one table for the sensors information with:
  + A column for the sensor ID
  + A column for the foreign key of the dimension of the measure made by the sensor (optional)
  + A column for the foreign key of the location of the sensor (optional)
+ There must be a table for the checks with:
  + A column for the name of the checks
  + A column for the method of the checks
  + A required column
+ There must be a table for the alerts with:
  + A column for the type of the alert
  + A column for the level of the alert
  + A column for the comment
  + A column for the timestamp of the alert

The other tables and columns are documented in `doc/configuration`.

You will also need a RabbitMQ server to handle the alerts.

### Installation

To run and install the application, you will need:
+ `python` 3.9 or more
+ `pip`

To install the monitoring_tools:
```git clone https://gitlab.com/bios1/monitoring
cd monitoring/monitoring_tool
pip install .
```

Configuration files should be in `~/.config/monitoring_tool/`

To launch the monitoring tools:
```
~/.local/bin/monitoring_tool <database_name>
```
Where database_name is the name of the database. The file 
`~/.config/monitoring_tool/<database_name>.txt` should exist.

### Configuration

The configuration files can be found in `~/.config/monitoring_tool/`. There are:
+ `__server_config__.txt` which is the configuration of the database connection
+ `<database>.txt` which is the configuration of the tables and columns of the
database. A template file can be found named `template.txt`. You can copy this
file, rename it with the name of your database (e.g. if your database is named
cool_store, rename it cool_store.txt) and fill the file.
+ `rabbitmq.credentials` are the credentials of your RabbitMQ server
+ `email_sender.credentials` are the credentials of the email used to send
alerts

The configuration process is detailed in `doc/configuration`.

### Usage

To run the checks on your database, you must run the python program with the
database name as an argument. If you installed the software locally by following
the instruction in *Installation*, it should be located in `~/.local/bin/` but
you can install it anywhere.

Running `monitoring_tool <database_name>` will perform the checks on your data
once. We recommend to set up a scheduling task to run the checks.

## Context

This repository is dedicated to the internship of the Nimubs Research Center "Monitoring and diagnostics tool for IoT Platforms".  
The internship started on April 1st 2021 and is supervised by Dr. Alex Vakaloudis.  
The topic of the internship revolves around the detection of errors for IoT devices.  
Interns:
 + Axel Comparetto-Berthier (01/04/2021 - 15/07/2021)
 + Clément Leboulenger (08/06/2021 - 06/08/2021)
 + Lorette Daussy (08/06/2021 - 06/08/2021) 

## Maintainers

+ Clément Leboulenger - clement.leboulenger@pm.me
+ Lorette Daussy - lorette.daussy@gmail.com