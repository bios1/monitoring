Configuration
=============

All the configuration files are located in `~/.config/monitoring_tool/`.
The folder is populated upon installation of the monitoring tool software.

Database configuration
----------------------
In order to use the monitoring application, your database needs to have several
tables and columns. The possible tables and columns are the following:

   | database : The name of the database
   |
   | version_sensitive : 1 if the database is version sensitive, 0 otherwise
   | version_table : The table where the version information are located
   | version_value : The column of the version of the database
   | version_timestamp : The column of the date of release of the database
   |
   | sensorDimension_table : The table of the dimensions of the measures (optional)
   | sensorDimension_pk : The foreign key of the dimension of the measure
   | sensorDimension_id : The ID of the dimension
   | sensorDimension_name : The name of the dimension (e.g. temperature)
   | sensorDimension_symbol : The symbol of the dimension (e.g. °C)
   |
   | sensorInstance_table : The table where the sensors information are located
   | sensorInstance_pk : The primary key of the sensor
   | sensorInstance_id : The ID of the sensor
   | sensorInstance_fk_sensorDimension : The foreign key of the dimension of the sensor
   |
   | sensorValues_table : The table where the values of the measures are located
   | sensorValues_pk : The primary key of the values
   | sensorValues_fk_sensorInstance : The foreign key of the sensor instance which made the measure
   | sensorValues_value : The value of the measure
   | sensorValues_timestamp : The date of the measure
   |
   | alerts_table : The table where the alerts are inserted
   | alerts_pk : The primary key of the alerts
   | alerts_type : The type of alert
   | alerts_level : The level of severity of the alert
   | alerts_status : The status of the alert
   | alerts_timestamp : The timestamp of when the alert was raised
   | alerts_fk_sensorInstance = The foreign key of the sensor which raised the alert
   | alerts_comment : The comment of the alert
   |
   | checks_table : The table of the checks configuration. There must be one row for every checks
   | checks_pk : The primary key of the check
   | checks_codeclass : The method of the check. Usually, checks.class.method
   | checks_name : The name of the check
   | checks_req : Whether the check is required or not
   |
   | check_configuration_table : The table to configure the checks on a given sensor
   | checkConfiguration_pk : The primary key of the configuration
   | checkConfiguration_fk_checks : The foreign key of the check to perform
   | checkConfiguration_dimension : The dimension of the measure of the sensor to perform the check on
   | checkConfiguration_instance : The sensor to perform the check on
   | checkConfiguration_fk_location : The foreign key of the location of the sensors to perform the check on
