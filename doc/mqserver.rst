mqserver package
================

.. toctree::
   :maxdepth: 4

   util
   receive_logs_email
   receive_logs_email_period
   insert_in_database