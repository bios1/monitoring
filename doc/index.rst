.. monitoring documentation master file, created by
   sphinx-quickstart on Fri Jun 18 14:35:24 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to monitoring's documentation!
======================================
Monitoring tools for IoT devices database
-----------------------------------------

On this page you will find the documentation for the :doc:`monitoring_tool`,
which provide the tools to check IoT data, and the :doc:`mqserver`,
which are the rabbitmq consummers handling the alerts created by the checks.

.. toctree::
   :maxdepth: 2
   :caption: Contents

   modules
   configuration


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
