[[_TOC_]] <!-- should display table of contents -->

# Package `core`



## Module `constants` (file `constants.py`)

This file contains the code that initializes the global variables. Those variable have the prefix `glob_`.  
For the current version, the globale variables are:
 + `glob_str_tz_exec` and `glob_str_tz_dest`, which are strings corresponding to the timezones for (respectively) the code execution and the database timestamps. These variables are used when computing the local time for the database, when processing timestamps for example.  
 + `glob_target_db` is the name of the database that will be worked on on the distant server. Actually, it is the filename representing the database property file, but they are conventionnaly named the same.  

### Imports required

This file requires the import of `pytz` to handle timezones.  

### Function `setTimezones(auto=False)`

This function sets the global variables `glob_str_tz_exec` and `glob_str_tz_dest` to the correct timezones among `pytz.common_timezones`. The timezones are usually something like `{continent}/{city}`.  
If `auto=False`, the user sets manually the timezones, else the timezones are set as intended by the developper. Regardless, this function will be improved to a more user-friendly one thanks to the GUI.  





## Module `util` (file `util.py`)

This file contains short functions that serve generic purposes.  

### Imports required

This file requires the import of `numpy` to handle arrays, `pandas` to manipulate dataframes (both are imported under their respective alias `np` and `pd`) and timestamps.  
It also requires `pytz` to manage timezones and `datetime` to get current OS time.  

### Standalone functions 

**`bounds(mini, value, maxi)`**  

This function returns the bound value of `value` in the interval `[mini, maxi]`.  

**`getLocalTime(str_tz_exec, str_tz_dest, printResult=False)`**  

This functions returns a pandas timestamp object of the current time for the database given the timezone of code execution and the database timezone.  
This function is called when generating an alert (e.g., an alert is created by the programm, and needs to be inserted in a database with the database local time), or when checking the validity of timestamps. Its arguments are often the global variables `glob_str_tz_exec` and `glob_str_tz_dest`.  





## Module `binding` (file `binding.py`)

This file contains the code that makes the connection between the Python programm and the database.  

### Imports required

This file requires to install and import the library `connector` from the module `mysql` (thus the line `import mysql.connector`).  
This means the application is bound to run on MySQL servers only. For uses on other servers, one might need an implementation with `pyodbc`.  
This implementation remains commented in the code, as it might be more generic, but less portable, because it requires a few changes in the code and it requires a driver installation.  
This file aslo needs `pandas` and `numpy`, so they are imported with their usual aliases (respectively `pd` and `np`).  
This module needs the libraries `string` and `re` (regular expression) for string manipulation.  

### Class `DBProperty`

This class describes a database on a distant server, by containing its table and column names in a text file.  
Objects of this class are often named `dbprop` in the code, and the text file storing the information is conventionally named after the database.  

#### Instance attributes

 + `initialized` is a boolean, desribing if the object has been initialized (i.e., a valid database has been used to instantiate). This boolean is used so no method is called on an uninitialized database, especially class methods.  
 + `field_map` is a dictionary with both keys and elements being strings. The keys are generic names, often formatted as `{table type}_{column type}`, and must not change between databases. The elements are the names of tables and fields for a given database.  

#### Class methods

**Constructor `__init__(filename)`**  

The constructor takes a filename as argument (both the directory and the extension are imposed in the code, respectively `./core/properties/` and `.txt`) and reads the file to initialize the `field_map`.  
If the initialization is successful, `initialized` is set to one. This constructor uses regex and string manipulation to:  
 + separate the names of databases and the generic names used in the code.  
 + allow for comments in the file, '#' being the character introducing a comment.  
 + prevent SQL injection. The characters allowed are letters, digits, underscores and `=`.  

**`display()`**  

Displays the `field_map` if the object is `initialized`, or a message stating that the object is not `initialized`.  

### Standalone functions

**Function `server_connection(dbprop)`**  

This function establishes a connection to a MySQL server using its name/IP and port.
It uses a `DBProperty` object as argument to have the name of the database, and it returns a `mysql.connector` object containing the server information. This object is required for any further SQL task.  
If a trusted connection exists, credentials are not required. If credentials are required, they are read in the file `./core/properties/__server_config__.txt`, the username being on the first line and password on the second.  

**Function `db_getLatestVersion(server_connection, dbprop)`**  

This function returns the ID of the latest version of the database, for databases that have such versionning. 
This function has two arguments: a `mysql.connector` object as argument to send the query, and a `DBProperty` object to know the names of the relevant tables and fields. It returns an integer.  
The output of this function will be used in every SQL query on tables that mention the version of the database.  

**Function `db_getDateVersion(server_connection, dbprop, version=None)`**  

This function returns a string indicating the release date of the version `version` of the database. In order to be used in SQL queries, this string and has format `%Y-%m-%d %H:%M:%S`.  
This function has two mandatory arguments: a `mysql.connector` object as argument to send the query, and a `DBProperty` object to know the names of the relevant tables and fields. It returns a string.  
`version` is an optionnal integer argument, that has default value `None`. If `value=None`, the latest version is used, else the release date of the version of ID `version` is used. If `version` is negative or greater than the latest version, a `ValueError` is raised.  
This function is used on databases that use versionning, so that only data produced since the latest version release is used.  





## Module `collecting` (file `collecting.py`)

### Imports required

This module heavily relies on `pandas` to read outputs of SQL queries and store them in dataframes and `numpy` for dealing with arrays of data. They are imported with their usual aliases (respectively `pd` and `np`).  
This file also requires `binding.py`, `util.py` and `constants.py`.  

### Class `Collector`

This class handles the identification of a sensor and the retrival of data.  

#### Instance attributes

Each instance of a `Collector` has several attributes.  
The following attributes are about the sensor itself: 
 + `sensor_instance_id` is the unique numerical ID of the sensor. 
 + `sensor_dimension` is numerical ID of the physical quantity measured by the sensor.  
 + `sensor_location` is the ID of the lowest (i.e. most precize) location of the sensor. In case of intricated locations (e.g. building, floor, room), the most precize is to be used.  
 + `identified` is a boolean that is true if the sensor has been exactly identified (i.e., exactly one row in the output of the query, the identification is neither failed nor ambiguous). This parameter is used in methods of the class to ensure one does not execute impossible operation (e.g., access the data for a sensor that has not been identified).  
 + `sensor_data`

The last attribute, `sensor_data`, contains the data measured by the sensor.  
This attribute is a numpy `2*n` matrix (with `n` the number of samples).  
For a given instance `sensor`, the timestamps of the measures can be accessed with `sensor.sensor_data[0,:]`. Please note that timestamps are of type `pandas._libs.tslibs.timestamps.Timestamp`, which allows operations and comparisons between timestamps.  
The values measured are accessed with `sensor.sensor_data[1,:]`. Values are floats that allow usual operations.  

#### Class methods

**`getInfo(targetID, conn, dbprop)`**  
This method takes the integer argument `targetID` and generates a SQL query that returns the ID of the sensor for the current version of the database and the physical quantity measured. If the output of the query is 1 row, the sensor has been exactly identified, `identified` is set at True, `sensor_id` and `sensor_dimension` are initialized.  
The arguments `conn` and `dbprop` are respectively a `mysql.connector` object that send the query to the server and a `DBProperty` object to know the names of the relevant databases and fields.  
Otherwise (if the output has 0 row, the `targetID` has no sensor in the current version of the database ; or it has multiple rows, the identification is ambiguous), `identified` is set at False and no other attribute is initialized.  
For version-aware databases, please note that this method only acknowledges sensors that have been defined in the current version of the database.  

**`getLocation(conn, dbprop)`**   
This method retrieves the location of the sensor, this location being a primary key in a table of locations, and sets `sensor_location`.  
The arguments `conn` and `dbprop` are respectively a `mysql.connector` object that send the query to the server and a `DBProperty` object to know the names of the relevant databases and fields.  
If the location unseccessful (either no location or multiple location, ambiguous), set `sensor_location = -1`.  

**`getData(conn, dbprop, sinceVersionRelease=True)`**  
This method is to be called after `getInfo`. If the sensor is `not identified`, the method exits without initializing any attribute.  
This method uses a SQL query to recover the data for the sensor, and store it in `sensor_data`. No check (e.g., for `NULL` values) is performed on the data.  
The arguments `conn` and `dbprop` are respectively a `mysql.connector` object that send the query to the server and a `DBProperty` object to know the names of the relevant databases and fields.  
The argument `sinceVersionRelease` (default value `True`) is used only for version-aware databases. If True, it restricts the outputs to the values measured after the release of the current version of the database, as it facilitates the processing task (e.g. if the sensors went off between versions).  

**Constructor `__init__(targetID)`**  
The constructor of the `Collector` class takes the integer argument `targetID`, and returns a `Collector` object for the sensor of ID `targetID`.  
This methods creates the appropriate `DBProperty` object `dbprop` and `mysql.connector` object `conn` to be passed to other methods.  
It successively calls `getInfo(targetID, conn, dbprop)`, then (if `identified == True` after `getInfo(targetID)`) `getLocation(conn, dbprop)` and `getData(conn, dbprop)`.  

**`display()`**  
This method has no argument since it is supposed to be called after the construction of the object. If sensor is `not identified`, the method exits without displaying anything.  
This method displays the numerical attributes of a sensor and some insight (number of samples, mean and standard deviation) on its data in the console. Its main purpose is debugging, as a more user-friendly display method will be featured in the GUI.  

### Class `UserCheckConfiguration`  

This class allows the user to choose which checks will be performed on specific sensors or types of sensor, so that only the relevant tests are performed.  
Please note that the checks are defined in the database, and that some of them may be mandatory.  

#### Class attributes

This class has class-wide attributes about checks:    
 + `checksRequired` is a numpy array of numerical booleans defining if the checks are required/mandatory.  
 + `checksNames` is a list of strings containing the full names of the tests.  
 + `nbChecks` is the number of checks available, i.e., the lenght of `checksNames` (and `checksRequired`).  

#### Static methods

**`getChecksClassAttributes()`**  
This method initializes the class attributes thanks to SQL queries.  
It creates a `mysql.connector` object and a `DBProperty`, and it returns a tuple containing respectively `checksRequired` and `checksNames`.  

#### Instance attributes

 + `sensor_instance_id` is the sensor instance ID of the sensor on which the configuration will be applied.  
 + `sensor_dimension` is a numerical attribute representing the type of sensor (e.g., a thermometer).  
 + `sensor_location` is the ID of the lowest (i.e. most precize) location of the sensor.
 + `checksEnabled` is a numpy array of length `nbChecks` of numerical booleans. The check of ID `i` (defined in the database) is enabled if `checksEnabled[i]==1`.  

#### Class methods

**Constructor `__init__(collector)`**  
Initializes the instance attributes, `sensor_instance_id`, `sensor_dimension` and `sensor_location` being directly copied from `collector`. When initialized, all the checks are enabled!  

**`userChoiceManual()`**  
This method allows the user to manualy select the checks that are to be performed thanks to console dialogue.  
This method acknowledge for required checks, and those can not be disabled through this method.  
This method is inteded to be used under developpement only, since a more user-friendly version of it will be made with a GUI.  

**`userChoiceFromDB()`**  
This method looks for an existing configuration that fits the sensor in the database, and copies this configuration in `checksEnabled`.  
This method successively looks for a configuration that fits `sensor_instance`, then `sensor_location` and finally `sensor_dimension`.
If such configurations are found, they are concatenated (which means the resulting configuration will be the sum of all these configurations) and copied into `checksEnabled`.  
If no suitable configuration is found, only the required cheks are enabled.  
This method returns an integer `ret` between 0 and 7. More precisely, ret is a three-digits binary number describing the level of specificity of the configuration found:
 + the most significant bit is set to 1 if a configuration is found for the instance
 + the middle bit is set to 1 if a configuration is found for the location
 + the least significant bit is set to 1 if a configuration is found for the dimension measured by the sensor
 + ret = 0 if no configuration is found.

**`display()`**  
This method displays the `UserCheckConfiguration`, displaying a list of `x (r),` where `x` equals 1 if the check is enabled and 0 otherwise, and `r` equals 1 if the check is required and 0 otherwise.  






## Module `processing` (file `processing.py`)

This file contains the code that performs the check on the data from a Collector object, and eventually raises an alert.  

### Imports required

This file requires the import of `constants`, `util`, `binding` and `collecting` as it reuses assets from these files.  
Furthermore, this file requires the import of `demuxchecks` to link the checks and their IDs.  
The standard libraries `numpy` and `pandas` are also required for handling data; as well as the libraries `string` and `re` (reguar expressions) for string manipulation.  

### Class `Alert`

This class represent an alert that can be raised after checking the data of a `Collector`.  

#### Class attributes

The limit for several instance attributes are class attributes: `maxStatus`, `maxLevel` and `maxLenComment` are boundaries for instance attributes (more detail below).  

#### Instance attributes

The instance attributes defining an `Alert` are:
 + `type`: the integer representing type of error encountered (e.g. no data, abnormal values, missing timestamps). The possible values are given in the documentation of the checks. The only alert type triggered in this file is the 0 type, which is triggered when a sensor is identified but has no data available.  
 + `level` is the level of the alert, bound between 0 and `maxLevel`. Each type of error has different levels.  
 + `sensor_instance_id` is the ID of the sensor that triggered the alert. It is directly taken from the `Collector` object.
 + `comment` is a string describing the alert. Its length is bound between 0 and `maxLenComment`. The comment is secured from SQL injection using regex, the characters allowed being letters, digits, whitespaces, underscores, parenthesis and `:`.  
 + `dateAlert` is the date and time **for the database** when the alert is generated. Initialized by `getLocalTime()` at the object creation.
 + `status` is the status of the alert (`0<=>unchecked`, `1<=>acknowledged`, `2<=>fixed` and `3<=>ignored`), initialized to 0 as this attribute is not related to the alert creation.
 + `userID` is the numerical ID of the user who ackwoledged (and fixed or ignored) the alert. Initialized to `None`.  
 + `periodStart` and `periodEnd` are the temporal boundaries of the data that raised the alert (after considering the version-awareness of the database). Both initialized to `None` by default (i.e., not particular boundaries considered). These parameters allow for checks to raise multiple Alerts of the same type regarding the same sensor, to narrow down the problem temporaly.  


<!-- not to be displayed here, but to be updated
| Alert.type    | Raised by check    | Error         |
| :----: | :----: |-------------------------------|
| 0      | 0      | The sensor has no data |
| 10 	 | 1      | The sensor has undefined timestamps      |
| 11 	 | 1      | The sensor has invalid timestamps (e.g., in the future)     |
| 20     | 2      | The sensor has missing values considering its period      |
| 21 	 | 2      | The sensor has too many values considering its period      |
| 22 	 | 2      | The sensor went off for a long time considering its period     |
| 30 	 | 3      | Boxplot test on the sensor has detected suspicious outliers |
| 40 	 | 4      | Normal distribution test on the sensor has detected suspicious outliers |
| 41 	 | 4      | Normal distribution test attempted on values that do not follow a normal distribution |
| 42 	 | 4      | Normal distribution test attempted but not enough samples to check normality |
| 50 	 | 5      | Derivative check detected values are supposed to increase, but some derivatives are negative |
| 51 	 | 5      | Derivative check detected values are supposed to decrease, but some derivatives are positive |
| 52 	 | 5      | Derivative check detected some derivatives have a suspiciously high absolute value |
| 53 	 | 5      | Derivative check detected values for which derivative changes sign and has high absolute value |

-->

#### Class methods

**Constructor `__init__(typeAlert, levelAlert, sensor_instance_id, comment="", periodStart=None, periodEnd=None)`**  

Initializes all the attributes. Those that are irrelevant to the processing (`status` and `userID`) have their initialization hard-coded. The others are initialized with the respective argument of the constructor.  
Please note that this constructor acknowledges for max values of the arguments, and that some arguments are not mandatory since they have a default value.  

**`editComment(newcomment, append=True)`**  

This method allows to edit `comment` with `newcomment` being appended (if `append==True`) or entirely replacing the existing comment (if `append==False`).  
This function acknowledges for `maxLenComment` so the comment is not too long, and filters the characters allowed using regex.  
This method is called by the constructor.  

**`getForeignKey(conn, dbprop)`**  

The table storing the alarm uses a foreign key (which is the ID primary key in the table defining the sensors instances) to define sensors instances. This functions returns this foreign key.  
The arguments `conn` and `dbprop` are respectively a `mysql.connector` object to send queries and a `DBProperty` object to know the names of the relevant tables and fields.  

**`checkRedundancyDB(conn, dbprop)`**  

This method checks that there is no similar alarm (same `sensor_instance_id` and same `type` and same temporal boundaries `periodStart` and/or `periodEnd` if not `None`) that is unchecked (`status = 0`) in the database, in order not to spam the end-user.  
The arguments `conn` and `dbprop` are respectively a `mysql.connector` object to send queries and a `DBProperty` object to know the names of the relevant tables and fields.  
It returns -1 if the alert is new, or the ID of the similar alert in the alert table if there is one.  

**`insertDB(conn, dbprop)`**  

If the alert is not redundant, this method inserts the alert in the database. It then returns `True` if the insertion is successful. 
If the alert is redundant, this method update the older alert (its level, comment and timestamp) and returns `False` because no insertion was made.  

**`display()`**  

This method displays the attributes of the alert object in the console. This method is for debugging purposes only, as a more user-friendly is to be implemented in the GUI.  

### Class `Processor`  

This class designates the object that analyses the data of a `Collector` object, and eventually raise alerts.  

#### Class attributes  

The number of checks available `nbChecks` is a class attribute, to prevent attempting a check with invalid ID (assuming check IDs are incremented).  
The maximum level of alert `maxLevel` is a class attribute, to avoid triggering alerts of an invalid level.  

#### Instance attributes  

The class `Processor` has several attributes, most related to a number of alerts:  
 + `sensor_instance_id` is the ID of the sensor of which data is being processed  
 + `alertRaised` is a numpy array of `maxLevel` cells, all initialized at 0. This describes the number of alert raised per level, so that `alertRaised[i]` is the number of level `i` alerts triggered.  
 + `inserted` works the same way as `alertRaised`, but for alert insertion rather than alert triggering. `inserted[i]` is the number of level `i` alerts that have been inserted in the database.  
 + `checksDone` is an integer representing the number of individual checks performed on the `Collector`.  
 + `errors` is the number of Python errors that happened while processing the `Collector` (thus stopping the processing).  

#### Class methods  

**Constructor `__init__(collector)`**  

This constructor initializes the attributes. Please note that a `Collector` object is required to build a `Processor` object.  

**`raiseAlert(conn, dbprop, type, level, comment="", periodStart=None, periodEnd=None)`**  

Raises an alert for the `sensor_instance_id` being processed, the `type`, `level`, `comment`, `periodStart` and `periodEnd` (the last three being optionnal) of the alert are given as arguments.  
Tries to insert the alert in the database, and updates the `Processor`'s attributes in respect with the result of the insertion.  

**`display()`**  

Displays the attributes of the `Processor` in console. This method is for debugging purposes only, as a more user-friendly is to be implemented in the GUI.  

**`check(collector, ucc, demux)`**  

This methods checks if there is any data to process for the collector, and if there is, processes it by running checks.  
The checks that are performed are determined by `ucc.checksEnabled` (where `ucc` is a `UserCheckConfiguration` object specific to the collector).  
`demux` is a `DemuxChecks` object defining the methods to be called for the checks.  
If there is no data, raises an alert of type 0.  

**`checkDataExistence(collector)`**  

Check if `collector.sensor_data` is empty or not, i.e., is there any data to process? Returns `True` if the data exists, `False` otherwise.  
This check has the ID 0 and is not to be defined in the server's check database.  

**`executeCheck(checkID, collector, demux)`**  

This method is used to perform a check of given ID. If the ID is 0, `checkDataExistence` is performed, which is an inner check to the `Processor` method.  
For any other value of `checkID`, this function resorts to the `demux` method of the `DemxChecks` object `demux` to execute the right check.  
The check to be executed is in a `try... except` block so that an error does not crash the application.  





## Module `demuxchecks.py`

### Imports required

This file requires the import of `constants`, `util`, `binding` and `collecting` as it reuses assets from these files.  
The standard libraries `numpy` and `pandas` are also required for handling data.  
Most importantly, this file import **every** module of the subpackage `core.checks`.  

### Class `DemuxChecks`  

This class links the IDs of the checks (located in the distant database) and the code that is to be executed when performing a check.  

#### Instance attributes

 + `dictionary` is a dictionary representing the checks, the key being the numerical ID of the check, and the element being the adress of the Python method to be executed to run the check.  
 + `init` is a numerical boolean describing if the `dictionary` has been set. The dictionnary is set at the first call of the method `demux`.  

#### Class methods

**Constructor `__init__()`**  

This constructor initializes `dictionary` to an empty dictionary and `init` to 0.  

**`getDictionary(conn, dbprop)`**  

This methods reads the distant database to recover the IDs and method names of the checks available, then initializes `dictionary`.  
The arguments are respectively a `mysql.connector` object to send the queries and a `DBProperty` object to know the names of the relevant tables and fields.  
This method is automatically called by `demux` if `init == 0`.  

**`demux(checkID, processor, collector, conn, dbprop)`**  

This methods calls the right method to perform the check of ID `checkID`.  
It reads the adress of method to call in `dictionary` (eventually, initializes `dictionary` has it not been done before) and calls it with defined arguments: `method(processor, collector, conn, dbprop)`.
The arguments needed:
 + `processor` is a `Processor` object to have access to methods such as `raiseAlert`.  
 + `collector` is a `Collector` object to have acces to the data.  
 + `conn` and `dbprop` are respectively a `mysql.connector` object and a `DBProperty` object, used by methods such as `raiseAlert` while processing.  
Please note that this method imposes the arguments when calling an external check.  





## Subpackage `checks`

### About this subpackage

This subpackage contains the code files for every check available. The class `DemuxChecks` binds the checks and the main class `Processor`.  
Every module of this subpackage contains all the code required for one specific check. The purpose of this design is that anyone can add a check to the programm simply by writting it in its own file, without worrying about the interactions with the main classes.  

**Import chain**  

The main class `Processor` imports the file `demuxchecks.py` to link IDs of checks with the actions associated.  
`demuxchecks.py` then imports all the files of the package i.e., all the checks.  
To avoid a circular import chain, the check classes do not inherit from `Processor`, but instead use a `processor` object as argument.  

**How to implement a new check?**  

Checks are currently implemented through the following steps:  
 + Create a new file and save it in the package's folder, and name it after the check.  
 + This new file can import `constants`, `binding` and `util` if needed, however `processing` must not be imported (to prevent circular import).
 + Write a class named after the check. This class does not need any attribute to work with the core classes, so the constructor can be empty.  
 + Write a method of the class that performs the check. This method should take both the `processor` and the `collector` objects as argument. The check method thus has access to the data collected, and can raise alerts during the check.  
 + If need be, any subsidiary method required by the check can be written in the class as long as they are called inside the check method and not required outside of the class.  
 + Write one static method (i.e. that does not need an existing instance of the class to be called) with arguments `processor` and `collector` (in that order) that sould: instantiate an object of the class and call the check method on this object. The name of the method is not imposed, but the pre-made check classes had it named `runCheck`.  
 + Ultimately, update the database so that the new check is found. The ID is incremental, please make sure to follow the naming convention for `monitor_class`, i.e. use the format `module.class.method`. The name is arbitrary, for end_user clarity.  

### Pre-made checks

For the current version, the following checks have been made: 
 + Timestamps validity check (located in the file `timestampsValidity.py`)  
 + Timestamps period check (located in the file `timestampsPeriod.py`)  
 + Boxplot check (located in the file `valuesBoxplot.py`)  
 + Normal distribution check (located in the file `valuesNormalDist.py`)  
 + Derivative check (located in the file `valuesDerivatives.py`)  

Those checks have been designed in an effort to maximize the precision of alerts (i.e., minimize the number of false alerts).  
The documentation of each check is available in a markdown file named after the check, in the same folder.  





# Pacakge `gui`

# `tests.py`

# `main.py`

