# Nimbus research center internship: Monitoring and diagnostics tool for IoT Platforms - Theoretical models

## Nominal use case scenario

The nominal use case scenario of the use of IoT devices is important to define the types of errors that can occur, which will be the main topic of this internship.  
For the sake of this diagramm, I considered a trivial network with only one end user and one device, but that should not affect the model's genericity.  

**Actors**:
 + IoT device
 + Server
 + End user 

 Frame IoT device/Server -> Application: the end user shall see the application as a whole block rather than it containing the server and the device.  

**Actions**:

The device has a status, that covers both its electrical and network parameters. It can send a heartbeat to prove it is still functionnal, send its status upon request or send raw data. Some devices can receive orders from the end user via the server.  
The server acts as an interface between the end user and the device. It transmits information from one to the other (in both directions) and can also process information (e.g. data fixes, or change format from the end user order to make it understandable for the device).  
The end user can request information from the device (data or status) and send orders to some devices.

 + IoT device: send heartbeat, send data, send status, receive order
 + Server: receive heartbeat, receive data, request data, receive electrical status, process data, send data, receive order, process order, send order
 + End user: request data, receive data, send order, request status

**Dependencies/inclusions**:

ActionA extends ActionB: ActionA is sometimes a trigger for ActionB.  
ActionA includes ActionB: ActionA mandatorily triggers ActionB.  

 + End user requesting data extends server requesting data and sending data
 + Server requesting data extends device sending data
 + Server requesting status includes device sending status

<!-- TODO: incomplete + make a table -->

## Statechart of communication between an IoT device and its server

## Components definition

## Errors and fixes