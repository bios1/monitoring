# this is a comment, comments can also be added at the end of a code line
# this file is an empty template of what a database property should be
# please do not change the left hand side of the equalities
# the formatting is as: 
# 	{generic field}_table is a table name in SQL queries
#	{generic field}_pk is the primary key column name for the table
#	{generic field}_{specific field} is a column name for the table {generic field}_table
#	{generic1}_fk_{generic2} is the column name for the foreign key in the table generic1 to the primary key of generic2

database = 

version_sensitive = #is a boolean, 0 or 1, if 0 the fields for 'version' are ignored
version_table = 
version_value = 
version_timestamp = 

sensorDimension_table = 
sensorDimension_pk = 
sensorDimension_id = 
sensorDimension_name = 
sensorDimension_symbol = 

sensorInstance_table = 
sensorInstance_pk = 
sensorInstance_id = 
sensorInstance_fk_sensorDimension = 

sensorValues_table = 
sensorValues_pk = 
sensorValues_fk_sensorInstance = 
sensorValues_value = 
sensorValues_timestamp = 

alerts_tale = 
# alert table structure is determined by the programmer, and identical for every database

checks_table = 
# checks table structure is determined by the programmer, and identical for every database

check_configuration_table = 
# check configuration table structure is determined by the programmer, for identical in every database