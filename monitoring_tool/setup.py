#!/usr/bin/env python

from setuptools import setup
from setuptools.command.develop import develop
from setuptools.command.install import install
from distutils.core import setup
import os
from shutil import move

class PostDevelopCommand(develop):
    """Post-installation for development mode."""
    def run(self):
        develop.run(self)
        post_install()


class PostInstallCommand(install):
    """Post-installation for installation mode."""
    def run(self):
        install.run(self)
        post_install()

server_config_file_tmp = '{}/.config/monitoring_tool/tmp/'.format(os.environ['HOME'])
server_config_file = '{}/.config/monitoring_tool/'.format(os.environ['HOME'])

def post_install():
    if not os.path.exists(server_config_file+"__server_config__.txt"):
        move(server_config_file_tmp+"__server_config__.txt", server_config_file)
    
    os.remove(server_config_file_tmp+"__server_config__.txt")
    os.rmdir(server_config_file_tmp)


setup(name='monitoring_tool',
      version='0.1',
      description='Monitoring tool for iot devices',
      author='Axel Comparetto-Berthier, Lorette Daussy, Clément Leboulenger',
      author_email='a.comparettoberthier@etu.emse.fr, lorette.daussy@gmail.com, clement.leboulenger@pm.me',
      url='https://gitlab.com/bios1/monitoring',
      install_requires=[
          'pandas', 'numpy', 'matplotlib', 'pytz', 'mysql-connector', 'sklearn', 'rrcf', 'pika'
      ],
      packages=['monitoring_tool', 'monitoring_tool.checks'],
      entry_points= {
          'console_scripts': [
              'monitoring_tool=monitoring_tool.main:main'
              ]
          },
       data_files=[
           (server_config_file_tmp, ['config/__server_config__.txt']),
           ('{}/.config/monitoring_tool/'.format(os.environ['HOME']),['config/template.txt'])
        ],
        cmdclass={
        'develop': PostDevelopCommand,
        'install': PostInstallCommand,
        }
     )
