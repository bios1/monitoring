#====================================
#        Header, import modules
#====================================

from monitoring_tool import constants
from monitoring_tool import util
from monitoring_tool import database
from monitoring_tool import collecting
from monitoring_tool import processing

import pandas as pd
import numpy as np

#====================================
#        All around functions for testing
#====================================

def validIDSensors(database, vers=None):
    query = ("SELECT DISTINCT({t1}.{c1})"
            " FROM {t1} JOIN {t2}"
            " ON {t2}.{fk21} = {t1}.{pk1}")
    if(database.properties["version_sensitive"] == '1'):
        query += " WHERE {t1}.{c2} = %s AND {t2}.{c3} > %s"
        query = query.format(t1 = database.properties["sensorInstance_table"],
                             t2 = database.properties["sensorValues_table"],
                             c1 = database.properties["sensorInstance_id"],
                             fk21 = database.properties["sensorValues_fk_sensorInstance"],
                             pk1 = database.properties["sensorInstance_pk"],
                             c2 = database.properties["sensorInstance_version"],
                             c3 = database.properties["sensorValues_timestamp"] )
        if vers is None:
            vers = database.version
            date = database.version_date
        else:
            date = database.query_version_date(vers)
        param = (vers, date)
        sensors = pd.read_sql_query(query, database.conn, params=param)
    else:
        query = query.format(t1 = database.properties["sensorInstance_table"],
                             t2 = database.properties["sensorValues_table"],
                             c1 = database.properties["sensorInstance_id"],
                             fk21 = database.properties["sensorValues_fk_sensorInstance"],
                             pk1 = database.properties["sensorInstance_pk"] )
        sensors = pd.read_sql_query(query, database.conn)
    sensors = sensors.transpose()
    sensors = sensors.to_numpy()
    sensors = list(sensors[0])
    return sensors
    
#====================================
#        demuxcheck dictionnary test
#====================================

def demuxcheck_dictionary():
    demuxchecks.getDictionary()
    
#====================================
#        test for params in queries
#====================================    

def queryParam():
    dbprop = binding.DBProperty("bios")
    conn = binding.server_connection(dbprop)
    cursor = conn.cursor()
    
    #ok
    print("Query 1:")
    try:
        query = "SELECT * FROM monitor_test WHERE id=%s"
        idTarget = 5
        params = (idTarget,) #comma required to have a tuple
        temp = pd.read_sql_query(query, conn, params=params)
        print(temp)
    except:
        print("Query 1 failed")
    print("\n")
    
    print("Query 2:")
    try:
        query = "SELECT * FROM monitor_test WHERE name=%s"
        nameTarget = "Derivative check"
        params = (nameTarget,) #comma required to have a tuple
        temp = pd.read_sql_query(query, conn, params=params)
        print(temp)
    except:
        print("Query 2 failed")
    print("\n")
    
    #not the result expected because fields and table names are not meant to be passed like this
    print("Query 3:")
    try:
        query = "SELECT %s FROM monitor_test WHERE id=%s"
        col_name = "id, monitor_class, name, required"
        condition = 5
        params = (col_name, condition,)
        temp = pd.read_sql_query(query, conn, params=params)
        print(temp)
    except:
        print("Query 3 failed")
    print("\n")    
    
    print("Query 4:")
    try:
        tab_name = "monitor_test"
        query = "SELECT * FROM {t}"
        query = query.format(t=tab_name)
        print(query)
        temp = pd.read_sql_query(query, conn)
        print(temp)
    except:
        print("Query 4 failed")
    print("\n")
    
    print("Query 5:")
    try:
        tab_name = "monitor_test"
        col_names = "name"
        condition_col = "id"
        condition_val = 5
        query = "SELECT {c} FROM {t} WHERE {cond}=%s"
        query = query.format(c=col_names, t=tab_name, cond=condition_col)
        params = (condition_val,)
        print(query)
        temp = pd.read_sql_query(query, conn, params=params)
        print(temp)
    except:
        print("Query 5 failed")
    print("\n")
    
    print("Query 6:")
    try:
        tab_name = "monitor_test"
        col_names = ["id", "name", "required"]
        condition_col = "id"
        condition_val = 5
        query = "SELECT {c} FROM {t} WHERE {cond}=%s"
        query_formatted = query.format(c=','.join(col_names), t=tab_name, cond=condition_col )
        params = (condition_val,)
        print(query_formatted)
        temp = pd.read_sql_query(query_formatted, conn, params=params)
        print(temp)
    except:
        print("Query 6 failed")
    print("\n")
    
    conn.close()
    
def periodTest():
    dbprop = binding.DBProperty("elighthouse_building")
    conn = binding.server_connection(dbprop)
    id = 2083190
    collector = collecting.Collector(id)
    collector.display()
    processor = processing.Processor(collector)
    demux = demuxchecks.DemuxChecks()
    processor.executeCheck(2, collector, conn, dbprop, demux)
    processor.display()
    
def RRCFTest():
    dbprop = binding.DBProperty("bios")
    conn = binding.server_connection(dbprop)
    id = 17
    collector = collecting.Collector(id)
    collector.display()

#====================================
#        batch test
#====================================

def batchTest(db):
    vers = 21 #version to be considered, None if latest version
    listIDs = validIDSensors(db, vers)
    checks = 0
    alerts = 0
    inserted = 0
    custom_conf = 0
    errors = 0
    
    if( len(listIDs) >= 20 ):
        listIDs = listIDs[0:19]
    
    for index, id in enumerate(listIDs):
        
        print(index+1, "/", len(listIDs))
        
        #building objects
        processor = processing.Processor(db, id)
        processor.check()
        
        #updating statistics
        processor.display()
        checks += processor.checksDone
        errors += processor.errors
        alerts += len( processor.alertsRaised )
        inserted += sum(processor.getInsertions()).item()
        #if(retconf != 0): #doable: detail the levels of configuration with the i-th binary digit of retconf (retconf//(2**i))%2 (with i in [0, 2])
        #    custom_conf += 1
        
    #comment editing
    newcomment = "'" + str(checks) + " checks on " + str(len(listIDs))
    newcomment += " sensor instances (used " 
    newcomment += str(custom_conf) 
    newcomment += " custom check configurations)"
    newcomment += " have raised " + str(alerts) + " alerts, "
    newcomment += str(inserted) + " of which caused an insertion in database "
    newcomment += "(" + str(errors) + " errors while processing)'"
    print(newcomment)
    
    #insertion in db
    existing = alerts - inserted
    date = util.getLocalTime(constants.glob_str_tz_exec, constants.glob_str_tz_dest)
    fmt = '%Y-%m-%d %H:%M:%S'
    str_date = date.strftime(fmt)
    param = (newcomment, str_date, existing, inserted, alerts, checks)
    query = ("INSERT INTO {t} ({pk}, {c1}, {c2}, {c3}, {c4}, {c5}, {c6}) "
            "VALUES(DEFAULT, %s, %s, %s, %s, %s, %s);" )
    dbprop = db.properties
    query = query.format(t = dbprop["log_table"],
                        pk = dbprop["log_pk"],
                        c1 = dbprop["log_comment"],
                        c2 = dbprop["log_timestamp"],
                        c3 = dbprop["log_oldalerts"],
                        c4 = dbprop["log_newalerts"],
                        c5 = dbprop["log_nbalerts"],
                        c6 = dbprop["log_nbchecks"] )
    conn = db.conn
    cursor = conn.cursor()
    cursor.execute(query, param)
    conn.commit()
    
#====================================
#        location test
#====================================

def locationTest(db):
    #gets valid sensors
    vers = 21
    listIDs = validIDSensors(db, vers)
    print("List of valid IDs: ", listIDs)
    print("Total number of valid sensors: ", len(listIDs))
    
    #identification of the optimal grouping (i.e. most sensors)
    groupByLocation = True
    groupByDimension = False
    list_collectors = optimalTestGroup(listIDs, db, vers, groupByLocation, groupByDimension)
    target_location = list_collectors[0].sensor_location*groupByLocation + (-1)*(not groupByLocation)
    target_dimension = list_collectors[0].sensor_dimension*groupByDimension + (-1)*(not groupByDimensions)
    print("Best found configuration (location, dimension) = (", target_location, ",", target_dimension, ")" )
    print("Found an optimal testing group of", len(list_collectors), "sensors")
        
def optimalTestGroup(listIDs, db, vers=None, groupByLocation=True, groupByDimension=False):
    """
    Identifies the optimal (more populated) test group for a batch check
    Arguments: a list of IDs of valid sensors, a Databse, a target version and booleans defining grouping conditions
    Returns: a list of Collectors representing the optimal test group
    """
    list_collectors = []
    
    list_configurations = [] #holds all the configurations encountered, shares indexes with count_configuration
    count_configurations = [] #holds the count of each configuration encountered, shares indexes with count_configuration
    
    for id in listIDs:
        collector = collecting.Collector(db, id, vers, dataRetrieval=False) #does not retrieve data immediately to save memory
        #sets the grouping configuration the sensor belongs to (acknoledges for the function arguments)
        current_grouping_conf = [collector.sensor_location, collector.sensor_dimension]
        if not groupByLocation:
            current_grouping_conf[0] = -1
        if not groupByDimension:
            current_grouping_conf[1] = -1
            
        #updates list_configuration and count_configuration
        if(current_grouping_conf != [-1, -1]): #does not consider unidentified/default configurations
            try:
                index = list_configurations.index(current_grouping_conf) #ok because there is at most one identical configuration in list_configuration
            except ValueError: #the current configuration is not in the config list, add it
                list_configurations.append( current_grouping_conf )
                count_configurations.append(1)
            else:
                count_configurations[index] += 1
        
    #gets optimal configuration
    optimal_index = count_configurations.index( max(count_configurations) ) #index of the most populated configuration
    optimal_configuration = list_configurations[ optimal_index ]
    
    #fills list_collectors with all sensors corresponding to the optimal configuration
    for id in listIDs:
        collector = collecting.Collector(db, id, vers, dataRetrieval=False) #does not retrieve data immediately to save memory
        current_grouping_conf = [collector.sensor_location, collector.sensor_dimension]
        if not groupByLocation:
            current_grouping_conf[0] = -1
        if not groupByDimension:
            current_grouping_conf[1] = -1
        if( current_grouping_conf == optimal_configuration ):
            collector.getData(vers) #retrieves the data of the collector
            list_collectors.append( collector )
            
    return list_collectors
        
        
    
        
        
    