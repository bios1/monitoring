#====================================
#        Header, import modules
#====================================

import pytz

#====================================
#        Global variables
#====================================

glob_str_tz_exec = ""
glob_str_tz_dest = ""
glob_target_db = ""

#====================================
#        Functions that modify global variables
#====================================

def setTimezones(auto=False):
    """Function to set global variables of timezones.
    
    This method allows to set timeszones if the processing does not
    run in the same place as the collecting (e.g., the data has been collected
    at 12:00 UTC, and processed at 11:00 UTC-2, that would not make sense to
    say that alerts have been raised at 11:00 for data collected seemingly later)
    
    Args:
        auto (bool) : for developping purpose, the timezones have been
                    automatically selected to suits the developers' situation.
                    
    Note:
        The timezones are selected among :obj:`pytz.common_timezones`, which is
        usually in the format "continent/city".
    """
    global glob_str_tz_exec
    global glob_str_tz_dest
    if(auto):
        glob_str_tz_exec = "Europe/Paris"
        glob_str_tz_dest = "Europe/Dublin"
    else:
        print("Here are the timezones available:")
        print(pytz.common_timezones)
        print("In which timezone is the code executed: ")
        temp = input()
        while not (temp in pytz.common_timezones):
            print("This timezone is not defined. Please insert valid timezone: ")
            temp = input()
        glob_str_tz_exec = temp
        print("In which timezone is the database: ")
        temp = input()
        while not (temp in pytz.common_timezones):
            print("This timezone is not defined. Please insert valid timezone: ")
            temp = input()
        glob_str_tz_dest = temp