#====================================
#        Header, import modules
#====================================

import os, sys, traceback
import pandas as pd
import numpy as np

from .database import Database
from .processing import Processor

#====================================
#        Class GroupProcessor
#====================================

class GroupProcessor:
    """Processor Class, manages the data checking process for sensor groups
    
    This class executes the checks assigned to each group of sensors. Those
    checks do not raise Alerts at the moment.

    Args:
        database (:py:class:`Database`) : Database in which the collectors
                                          gather data.
        processors (list of :py:class:`Processor`) : list of all the processors
                                                    available for the database.
        filter (list of ints) : list of two ints defining the filtering criteria
                                to constitute the group of sensors.
        checks (list of ints) : the IDs of the checks to be executed on the 
                                group of sensors.

    Attributes:
        database (:py:class:`Database`) : Database in which the collectors
                                          gather data.
        group_checks_dict (dict) : Dictionary containing the methods 
                                      corresponding to the different checks that
                                      can be executed. Keys are the checks IDs.
        all_processors (list of :py:class:`Processor`) : list of all the processors
                                                    available for the database.
        location_filter (int) : location ID criterion to for a sensor to be part of
                                the group, is equal to `filter[0]`
        dimension_filter (int) : physical dimension ID criterion to for a sensor
                                to be part of the group, is equal to `filter[1]`.
        checksID (list of ints) : the IDs of the checks to be executed on the 
                                group of sensors.
        group  (list of :py:class:`Processor`) : list of the Processors
                                                constituting the group of sensors
                                                to be tested.
        checksDone (int) : the total number of checks performed by the Processor.
        errors (int) : the total number of errors (program failures) that occured
                        during the checking process. An error does not necessarily
                        mean that the data is incorrect, but that something stopped
                        the execution during the checking process.
    """
    
    def __init__(self, database, processors, filter, checks):
        self.database = database
        self.group_checks_dict = self.query_group_checks()
        self.all_processors = processors
        self.location_filter = filter[0]
        self.dimension_filter = filter[1]
        self.checksID = checks
        self.group = self.create_group()
        self.checksDone = 0
        self.errors = 0
        
    def query_group_checks(self):
        """Query the different methods corresponding to checks in the database.

        Returns:
            dict: Dictionary where the keys are the checks IDs and the value are
                  the methods.
        
        Note:
            This method only consider group checks (mode = 2)
        """
        query = "SELECT {pk}, {c1} FROM {t} WHERE {c2} = %s"
        query = query.format(t = self.database.properties["checks_table"],
                            pk = self.database.properties["checks_pk"],
                            c1 = self.database.properties["checks_codeclass"],
                            c2 = self.database.properties["checks_mode"])
        mode_group_check = 2 #this dictionnary is initialized only with group checks
        param = (mode_group_check, )
        response = pd.read_sql_query(query, self.database.conn, params=param)
        response = response.to_numpy()
        dict = {}
        for i in range(0, response.shape[0]):
            full_name = response[i][1] #full_name = "module.class.method"
            module_name = "monitoring_tool.checks." + full_name.split('.')[0] #consider import from project root
            class_name = full_name.split('.')[1]
            method_name = full_name.split('.')[2]
            module = __import__(module_name, fromlist=[None]) #imports the appropriate module, fromlist is there to ensure the module itself is imported, not a parent file
            class_ = getattr(module, class_name)
            method = getattr(class_, method_name)
            dict[response[i][0]] = method
        return dict
        
    def create_group(self):
        """Creates the group of sensors to be checked
        
        Uses the :py:attr:`location_filter` and :py:attr:`dimension_filter` to
        constitute a group of sensors out of all the Processors available.
        
        Returns:
            list of :py:class:`Processor` : list of the Processors constituting
                                            the group of sensors to be tested.
        
        Note:
            The group returned can be empty.
        """
        group = []
        for proc in self.all_processors:
            location_match = (self.location_filter <= 0) or (proc.collector.sensor_location == self.location_filter) #boolean: is the sensor location matching the filtering criteria
            dimension_match = (self.dimension_filter <= 0) or (proc.sensor_dimension == self.dimension_filter) #boolean: is the sensor dimension matching the filtering criteria
            has_data = proc.executeCheck(0)
            if(location_match and dimension_match and has_data):
                group.append(proc)
        return group
        
    def executeChecks(self):
        """Perform checks on the group of sensors
        
        Uses a loop on checksID to execute each check on the group of sensors.
        
        Note:
            A watchdog prevents checking empty Collectors.
        """
        if( len(self.group) == 0):
            return
        for id in self.checksID:
            method = self.group_checks_dict.get(id)
            try:
                method(self)
            except Exception as err:
                print("Error while processing sensor group of", len(self.group), "sensors (filter [", self.location_filter, ",", self.dimension_filter, "]), on check", id)
                print("Error message: ", err)
                trace = traceback.format_exception(etype=type(err), value=err, tb=err.__traceback__)
                print("".join(trace))
                self.errors +=1
            else:
                self.checksDone += 1