#====================================
#        Header, import modules
#====================================

import os, sys, traceback

from .collecting import Collector, UserCheckConfiguration
from . import util

import numpy as np
import pandas as pd

import string
import re #regular expression

#====================================
#       class Processor
#====================================

class Processor:
    """Processor Class, manages the data checking process for sensor instances
    
    This class executes the checks assigned to each Collector, and stores the
    feedback for further user notification.

    Args:
        database (:py:class:`Database`) : Database in which the sensor gathers
                                            data.
        sensor_id (int) : The ID of the sensor to be processed.

    Attributes:
        Processor.nbChecks (int) : class attribute, the number of checks available
        Processor.maxLevel (int) : the maximum severity level of an :py:class:`Alert`
        Processor.mqchannel
        Processor.mqexchange
        collector (:py:class:`Collector`) : the Collector of :obj:`database` that
                                        corresponds to the :obj:`sensor_id`, and
                                        on which the checks are performed.
        sensor_instance_id (int) : The ID of the sensor to be processed.
        sensor_dimension (int) : ID of the physical quantity measured by the
                                 Collector. Initialized from the Collector.
        ucc (:py:class:`UserCheckConfiguration`) : the UserCheckConfiguration that
                                                    defines the checks that will
                                                    be executed by the Processor.
        alertsRaised (list of ints) : list of the number of alerts raised on the
                                    Processor. `alertsRaised[i] = n` means that
                                    n Alerts of level i have been raised.
        checksDone (int) : the total number of checks performed by the Processor.
        errors (int) : the total number of errors (program failures) that occured
                        during the checking process. An error does not necessarily
                        mean that the data is incorrect, but that something stopped
                        the execution during the checking process.
    """
    #class attributes
    nbChecks = UserCheckConfiguration.nbChecks
    maxLevel = 3
    mqchannel = None
    mqexchange = ""
    
    #constructor
    def __init__(self, database, sensor_id):
        self.collector = Collector(database, sensor_id)
        self.ucc = UserCheckConfiguration(self.collector)
        self.alertsRaised = [0 for _ in range(Processor.maxLevel + 1) ] # empty list that will contain the alerts
        self.sensor_instance_id = self.collector.sensor_instance_id
        self.sensor_dimension = self.collector.sensor_dimension
        self.checksDone = 0
        self.errors = 0
        
    def raiseAlert(self, type, level, comment="", periodStart=None, periodEnd=None):
        """Method that raises an Alert
        
        This method raises an Alert and inserts it into the messaging queue.
        
        Args:
            type (int) : the ID describing the type of Alert encountered, i.e.,
                        the kind of problem. Defined in the checks.
            level (int) : the severity level of the Alert, between 0 and
                        Processor.maxLevel.
            comment (string) : a string describing the problem more explicitely
                            and providing more details.
            periodStart (date) : the Alert has been raised while processing data
                                starting from this timestamp. Useful for redundancy
                                checks.
            periodEnd (date) : the Alert has been raised while processing data
                                starting from this timestamp. Useful for redundancy
                                checks.
                                
        Note:
            This function includes a redundancy check for the Alerts.
        """
        level = util.bounds(0, level, Processor.maxLevel)
        routing_key = str(self.sensor_instance_id)
        routing_key += "." + str(type)  # Type really should be string and not number
        routing_key += "." + str(level) # Level should also be string

        # Check if alert is redundant
        query = ("SELECT {pk} FROM {t}"
                " WHERE {ck} = \"{comm}\""
                " AND {tk} = {type}"
                " AND {lk} = {level}")

        query = query.format(t = self.collector.database.properties["alerts_table"],
                            pk = self.collector.database.properties["alerts_pk"],
                            ck = self.collector.database.properties["alerts_comment"],
                          comm = comment,
                            tk = self.collector.database.properties["alerts_type"],
                          type = type,
                            lk = self.collector.database.properties["alerts_level"],
                         level = level)

        resp = pd.read_sql_query(query, self.collector.database.conn)

        if len(resp) > 0:
            print("Alert already sent")
            return

        try:
            if Processor.mqchannel == None:
                raise Exception("No channel found")
            Processor.mqchannel.basic_publish(
                exchange=Processor.mqexchange,
                routing_key=routing_key,
                body=comment)
            # print(" [x] Sent %r:%r" % (routing_key, comment)) #DEBUG
        except Exception as e:
            print("Unable to send alert :")
            print(e)
        else:
            self.alertsRaised[level] += 1

    def display(self):
        """Displays the Processor in the console
        
        Displays the Alerts raised by the Processor, level by level, and displays
        the number of checks performed and errors that occured. For debuging
        purpose only.
        """
        nbAlerts = sum(self.alertsRaised)
        print("Processing for sensor_instance_id", self.sensor_instance_id, "(dimension", self.sensor_dimension, ") raised", nbAlerts, "alert(s) after", self.checksDone, "checks. (", self.errors, "errors while processing)")
        print("\n")
    
    def check(self):
        """Perform checks on the Collector
        
        This method reads the UserCheckConfiguration and executes the checks that
        are enabled by calling successive `executeCheck` methods.
        
        Note:
            A watchdog prevents checking empty Collectors.
        """
        if(self.executeCheck(0)): #there is data to check
            for i in range(1, self.ucc.checksEnabled.size): #range 1... because test 0 has already been executed
                if(self.ucc.checksEnabled[i] == 1):
                    print("Running check ", UserCheckConfiguration.checksNames[i])
                    self.executeCheck(i)
            
    def checkDataExistence(self):
        """Checks the existence of data in the Collector
        
        This methods checks the existence of data in the Collector, meaning
        the size of :py:attr:`collector.sensor_data`. If no data has been found,
        an Alert is raised.
        
        Returns:
            bool : `True` if there is data to process, `False` otherwise.
        """
        self.checksDone += 1
        dataExistence = (self.collector.sensor_data[1,:].size > 0)
        if not self.collector.identified:
            self.errors += 1
            raise Exception("Attempted processing of unidentified sensor")
        elif(dataExistence == False):
            newcomment = "No data found for sensor_instance_id " + str(self.sensor_instance_id)
            self.raiseAlert(0, 1, newcomment)
        return dataExistence
    
    def executeCheck(self, checkID):
        """This method executes one check at a time
        
        This method executes one check from the UserCheckConfiguration. Checks
        are performed in a try/except block, so that errors are collected and
        do not crash the whole program. Updates the statistics of the Processor.
        
        Args:
            checkID (int) : the ID of the check to be executed.
        
        """
        if(checkID == 0):
            try:
                dataExistence = self.checkDataExistence()
            except Exception as err:
                print("Error message: ", err)
                trace = traceback.format_exception(etype=type(err), value=err, tb=err.__traceback__)
                print("".join(trace))
                return False #an exception in checkDataExistence means there is no data to check
            else:
                return dataExistence
            #checkDataExistence is inner test, no need to update values of errors and checks done
        else:
            method = self.collector.get_method_from_dict(checkID)
            try:
                method(self, self.collector)
            except Exception as err:
                print("Error while processing sensor instance", self.sensor_instance_id, "check", checkID)
                print("Error message: ", err)
                trace = traceback.format_exception(etype=type(err), value=err, tb=err.__traceback__)
                print("".join(trace))
                self.errors += 1
            else:
                self.checksDone += 1
