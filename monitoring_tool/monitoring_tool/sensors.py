import pandas as pd
import numpy as np
from .database import Database
from .processing import Processor
from . import mqconnect
from .processing_group import GroupProcessor

class Sensors:
    """ Class that holds all the processors class and execute the checks
    
    Container class that manages the execution of the checks, both for instances
    and groups of sensors.
    
    Args:
        database (:py:class:`Database`) : the database that holds the sensors and
                                        checks.
        version (int) : the version to consider for the database. `None` for the 
                        latest version.
        rabbitmq_hostname
        rabbitmq_credentials
        rabbitmq_port
        rabbit_mq_exchange_name
        
    Attributes:
        database (:py:class:`Database`) : the database that holds the sensors and
                                        the checks.
        version (int) : the version to consider for the database. `None` for the 
                        latest version.
        processors (list of :py:class:`Processor`) : list of all the Processors
                                                    available for the database.
    """
    def __init__(self, database, rabbitmq_hostname, rabbitmq_credentials, rabbitmq_port="default", rabbitmq_exchange_name="check-alert", version=None):
        self.mq_channel = mqconnect.connect(rabbitmq_hostname, rabbitmq_credentials, rabbitmq_port, rabbitmq_exchange_name)
        Processor.mqchannel = self.mq_channel
        Processor.mqexchange = rabbitmq_exchange_name
        self.database = database
        self.version = version
        self.database.connect()
        sensors_id_list = self.get_valid_sensors()
        print("Valid sensors :", sensors_id_list)
        self.processors = []
        for i in sensors_id_list:
            print("Initializing sensor ", i)
            self.processors.append(Processor(self.database, i))
        self.database.disconnect()

    def run_tests_instances(self):
        """Runs the checks on every sensor instance """
        self.database.connect()
        for proc in self.processors:
            print("Running tests for sensor ", proc.sensor_instance_id)
            proc.check()
            proc.display()
        self.database.disconnect()

    def get_valid_sensors(self):
        """Retrieves the list of valid sensors from the database
        
        This method retrieves the list of valid sensors (sensors defined in the
        targeted version and that have data for that version) from the database.
        This is purely a SQL process.
        
        Returns:
            list of ints : list of the IDs of the valid sensors for the
                            current database.
        """
        query = ("SELECT DISTINCT({t1}.{c1})"
                " FROM {t1} JOIN {t2}"
                " ON {t2}.{fk21} = {t1}.{pk1}")
        if(self.database.properties["version_sensitive"] == '1'):
            query += " WHERE {t1}.{c2} = %s AND {t2}.{c3} > %s"
            query = query.format(t1 = self.database.properties["sensorInstance_table"],
                                 t2 = self.database.properties["sensorValues_table"],
                                 c1 = self.database.properties["sensorInstance_id"],
                               fk21 = self.database.properties["sensorValues_fk_sensorInstance"],
                                pk1 = self.database.properties["sensorInstance_pk"],
                                 c2 = self.database.properties["sensorInstance_version"],
                                 c3 = self.database.properties["sensorValues_timestamp"] )
            param = (self.database.version, self.database.version_date)
            print(query)
            print(param)
            sensors = pd.read_sql_query(query, self.database.conn, params=param)
        else:
            query = query.format(t1 = self.database.properties["sensorInstance_table"],
                                 t2 = self.database.properties["sensorValues_table"],
                                 c1 = self.database.properties["sensorInstance_id"],
                                fk21 = self.database.properties["sensorValues_fk_sensorInstance"],
                                pk1 = self.database.properties["sensorInstance_pk"] )
            sensors = pd.read_sql_query(query, self.database.conn)
        sensors = sensors.transpose()
        sensors = sensors.to_numpy()
        sensors = list(sensors[0])
        return sensors
    
    def query_group_test_configurations(self):
        """Queries the group checks to be performed in the database.
        
        This method queries the group check configurations in the database.
        Any criterion location/dimension associated to at least one check is 
        such configuration. The schedule of the checks is made so that every
        check suiting a single configuration is executed successively.
        
        Returns:
            dict : a dictionary representing the group checks to be executed.
                    The key is a tupple of ints representing the filtering
                    criterion (location, dimension), the element is a list of
                    ints being the IDs of the chekcs to be executed for the
                    configuration.
        """
        query = ("SELECT {t1}.{c1}, {t1}.{c2}, {t1}.{fk12} "
                " FROM {t1} JOIN {t2} "
                " ON {t1}.{fk12} = {t2}.{pk2} "
                " WHERE {t2}.{c3} = %s") #the last line of the query ensures we retrieve only configurations for group tests
        query = query.format(t1 = self.database.properties["checkConfiguration_table"],
                            t2 = self.database.properties["checks_table"],
                            c1 = self.database.properties["checkConfiguration_fk_location"],
                            c2 = self.database.properties["checkConfiguration_dimension"],
                            fk12 = self.database.properties["checkConfiguration_fk_checks"],
                            pk2 = self.database.properties["checks_pk"],
                            c3 = self.database.properties["checks_mode"] )
        mode_group_test = 2
        param = (mode_group_test, )
        group_tests_np = pd.read_sql_query(query, self.database.conn, params=param)
        group_tests_np = group_tests_np.to_numpy(dtype=int) #group_tests_np[i] = [location, dimension, test]
        group_tests_np = np.nan_to_num(group_tests_np) #replaces all the nan values with large negative value
        # so negative value in group_tests means that the column (either location or dimension) is not used for filtering
        
        #transform to dictionary, e.g a list of two configs [[1, 2, 3], [1, 2, 4]] would become {(1,2): [3, 4]}, so that group checks on the same sensors will be performed successively
        group_test = { }
        for config_np in group_tests_np:
            if(config_np[0] < 0):
                config_np[0] = -1
            if(config_np[1] < 0):
                config_np[1] = -1
            filter = (config_np[0], config_np[1]) #tupple because filter must be hashable to be a key, lists are not
            check = config_np[2]
            if filter in group_test: #check if the key is already in the dictionnary
                group_test[filter].append(check) #if yes, append the check to the list of checks for the same filter
            else: #if not, create a list of the only check yet to be executed for given filter
                group_test[filter] = [check]
        return group_test
    
    def run_tests_groups(self):
        """Runs the checks on every group of sensors """
        self.database.connect()
        group_tests = self.query_group_test_configurations() #dictionary, key=filter, element=list of tests to be run for the corresponding filter
        for filter in group_tests: #filter is the key
            checks = group_tests[filter]
            group_processor = GroupProcessor(self.database, self.processors, filter, checks)
            group_processor.executeChecks()
        self.database.disconnect()