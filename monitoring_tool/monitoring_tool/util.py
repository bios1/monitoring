#====================================
#        Header, import modules
#====================================

import numpy as np
import pandas as pd

import datetime
import pytz

#====================================
#       standalone functions 
#====================================
    
def bounds(mini, value, maxi):
    """Bounds a value in a given interval
    
    Args:
        mini (float) : the lower bound of the interval
        maxi (float) : the upper bound of the interval
        value (float) : the value to be bound in the interval [mini, maxi]
        
    Returns:
        float : the bound value of `value` in the interval [mini, maxi]
    """
    if(value < mini):
        return mini
    elif(value > maxi):
        return maxi
    return value
    
def getLocalTime(str_tz_exec, str_tz_dest, printResult=False):
    """Gets the local time in a given timezone
    
    Gets the local time in a timezone given the execution time in another
    timezone. All timezones are chosen among `pytz.common_timezones`.
    
    Args:
        str_tz_exec (string) : the timezone of execution.
        str_tz_dest (string) : the destination timezone, i.e., the one containing
                                the database.
        printResult (bool) : should the result be printed in console? Debuging
                            purpose only.
    
    Returns:
        date : the local time in the destination timezone, formatted as
                '%Y-%m-%d %H:%M:%S'
    """
    #for a list of available timezones, please type 'print(pytz.common_timezones)'
    tz_exec = pytz.timezone(str_tz_exec)
    tz_dest = pytz.timezone(str_tz_dest)
    fmt = '%Y-%m-%d %H:%M:%S'
    exec_time = tz_exec.localize(datetime.datetime.now())
    dest_time = exec_time.astimezone(tz_dest)
    if(printResult):
        print("Local time at code execution in zone", str_tz_exec, ": ", exec_time.strftime(fmt))
        print("Local time for the datatbase in zone", str_tz_dest, ": ", dest_time.strftime(fmt))
    dest_time = pd._libs.tslibs.timestamps.Timestamp(dest_time) #pandas timestamp conversion, as it is the unit used in the database
    dest_time = dest_time.tz_localize(None) #removes tz-awarness of the timestamp to allow comparison with timestamps of the database
    return dest_time