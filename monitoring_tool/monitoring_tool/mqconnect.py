import pika

def connect(host, credentials, port="default", exchange="check-alert"):
    """ Connect the program to the RabbitMQ server
        Return the channel to communicate with the server
        TODO: handler errors
    """ 
    if port == "default":
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=host, credentials=credentials))
    else:
        connection = pika.BlockingConnection(
            pika.ConnectionParameters(host=host,
                                      port=port,
                                      credentials=credentials))
    channel = connection.channel()
    channel.exchange_declare(exchange=exchange,
                             exchange_type="topic")
    return channel