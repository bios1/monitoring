# Timestamps period check   

## Imports required

This file requires the import of `constants`, `util`, `binding` and `collecting` as it reuses assets from these files, notably those related to timestamps and timezones.  
The standard libraries `numpy` and `pandas` are also required for handling data.  
The method `floor` is imported from the module `math`.   

## How does it work?

Located in the file `timestampsPeriod.py`.  
This check looks for abnormality in the `collector`'s timestamps given its computed period (the period is computed by the `getPeriod` method, details below).  
Said abnormalities can be of three types (thus three types of alert can be triggered): there are too many samples given the sensor's period and its activity duration, there are not enough samples given the sensor's period and its activity duration or the sensor probably went off for a long time (at least ten time its period).  

**`getPeriod(collector, threshold=0.9, tolerance=pd.Timedelta(3, unit="s"))`**  

This method aims to return the nominal period of the `collector`. In order to do so, it counts the number of identical time intervals between two successive measures.  
If for a time interval, this count is greater than `threshold` multiplied by the number of samples, it is returned as the nominal period. Returns `NaT` if no such period is found.  
In other words, if this function returns `period`, it means than at least `100*threshold`% of the time intervals between two successive measures are equal to `period`.  
Furthermore, if `threshold=1` (all the measures have the same period), any missing value will trigger this function to return `NaT`, therefore missing said error. To avoid if, `threshold` is bound to be lesser than 0.99.  
The method accounts for a tolerance, meaning that if `period2` is in the interval `[period1 - tolerance, period1 + tolerance]`, both the count of `period2` and `period1` will be incremented.  
If there is an equality for the periods count with the tolerance (and the periods' count is greater than `threshold` multiplied by the number of samples), the method return the period that has the most count without the tolerance.  
If there is still an equality, the method returns `NaT` as the period is ambiguous.  

**`getInterruptionDuration(interBegin, interEnd, boundLow=pd.NaT, boundHigh=pd.NaT)`**  

This method computes the interruption duration between `boundLow` and `boundHigh` given the lists of the interruptions beginning `interBegin` and end `interEnd` timestamps.  
If `boundLow` and/or `boundHigh` is `NaT`, all values available in `interBegin` and `interEnd` are used.  

**`checkTimestampsPeriod(processor, collector, conn, dbprop, slicing_unit="d")`**  

This method is the main check of the class.  
It first checks for activity interruption, i.e., large time spans when the sensor did not send data (greater than 10 times the period, hardcoded value for now).  
It then takes all the samples of the `collector`, slices them into sublists according to the `slicing_unit` parameter (default value: day) and for each sublist, checks if the number of values corresponds to the period (and considers the activity interruptions).  
If not, it raises an alert per slice that doesn't have the required number of values.  

## Check information

Check ID: 2  
Alert types that can be raised:  
| Alert.type    | Raised by check    | Error         |
| :----: | :----: |-------------------------------|
| 20      | 2      | The sensor has missing values considering its period      |
| 21 	 | 2      | The sensor has too many values considering its period      |
| 22 	 | 2      | The sensor went off for a long time considering its period     |
