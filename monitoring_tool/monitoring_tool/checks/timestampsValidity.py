#====================================
#        Header, import modules
#====================================

from .. import constants
from .. import util

import numpy as np
import pandas as pd

#====================================
#        class TimestampsValidity
#====================================

class TimestampsValidity:
    
    def __init__(self):
        return
    
    def checkTimestampsValidity(self, processor, collector):
        #all existing timestamps or some NULL? timestamps in the future?
        l_nat = []
        l_future = []
        nsamples = collector.sensor_data[0,:].size
        time_start_check = util.getLocalTime(constants.glob_str_tz_exec, constants.glob_str_tz_dest)
        for i in range(0, nsamples):
            if( pd.isnull(collector.sensor_data[0, i]) ): #must check for NaT timestamps first
                l_nat.append(i)
            elif(collector.sensor_data[0,i] > time_start_check):
                l_future.append(i)
        l_ts_pb = np.array(l_nat + l_future, dtype=int) #concatenation of both list for removal in the collector
        collector.sensor_data = np.delete(collector.sensor_data, l_ts_pb, 1) #removes both timestamps and values where timestamps have a problem
        if(len(l_nat) > 0):
            newcomment = "sensor_instance_id " + str(processor.sensor_instance_id) + " has " + str(len(l_nat)) + " undefined timestamps out of " + str(nsamples) + " samples"
            processor.raiseAlert( 10, 1, newcomment)
        if(len(l_future) > 0):
            newcomment = "sensor_instance_id " + str(processor.sensor_instance_id) + " has " + str(len(l_future)) + " timestamps in the future out of " + str(nsamples) + " samples"
            processor.raiseAlert( 11, 0, newcomment)
    
            
    def runCheck(processor, collector):
        currCheck = TimestampsValidity()
        currCheck.checkTimestampsValidity(processor, collector)