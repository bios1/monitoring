#====================================
#        Header, import modules
#====================================

from .. import constants
from .. import util

import numpy as np
import pandas as pd

from scipy.stats import shapiro
from scipy.stats import norm

#====================================
#        class ValuesNormalDist
#====================================

class ValuesNormalDist:
    
    def __init__(self):
        return
    
    def checkValuesNormalDist(self, processor, collector,  toleranceMultiplier=5):
        
        values = collector.sensor_data[1,:]
        nbsamples = values.size
        if nbsamples < 30: #not enough samples to check normality
            newcomment = "Normal distribution check on sensor_instance_id " + str(processor.sensor_instance_id)
            newcomment += " attempted but not enough samples to check for normal distribution " 
            newcomment += "(" + str(nbsamples) + " samples)"
            processor.raiseAlert( 42, 0, newcomment)
            return
            
        values.sort()
        stats, p = shapiro(values) #checks the normality of the data
        if p < 0.05: #the data in the confidence interval is not normally distributed
            newcomment = "Normal distribution check on sensor_instance_id " + str(processor.sensor_instance_id)
            newcomment += " attempted but values do not follow normal distribution " 
            processor.raiseAlert( 41, 0, newcomment)
            return
        
        mean = values.mean()
        std = values.std()
        threshold = 1/(toleranceMultiplier*nbsamples) #probability under which the value is considered an outlier
        outliers_low = 0 #number of outliers lower than lowerLim
        outliers_up = 0 #number of outliers greater than upperLim
        
        index = 0 #starts from the lowest index, increasing up to the confidence lower limit
        cumulative_prob = norm.cdf(values[index], loc=mean, scale=std)
        while(cumulative_prob < threshold):
            index += 1
            cumulative_prob = norm.cdf(values[index], loc=mean, scale=std)
            outliers_low += 1
        index = nbsamples-1 #strats from the highest index, decreasing up to the confidence upper limit
        cumulative_prob = norm.cdf(values[index], loc=mean, scale=std)
        while(cumulative_prob > 1-threshold):
            index -= 1
            cumulative_prob = norm.cdf(values[index], loc=mean, scale=std)
            outliers_up += 1
        
        outliers = outliers_low + outliers_up
        if(outliers > 0):
            newcomment = "Normal distribution check on sensor_instance_id " + str(processor.sensor_instance_id)
            newcomment += " has detected " + str(outliers) + " suspicious outliers"
            newcomment += " (" + str(outliers_up) + " upper values, " + str(outliers_low) + " lower values)"
            processor.raiseAlert( 40, 2, newcomment)
            
    def runCheck(processor, collector):
        currCheck = ValuesNormalDist()
        currCheck.checkValuesNormalDist(processor, collector)
        
    ##former implementation below
    # def checkValuesNormalDist(self, processor, collector, dataConfidence=0.90, toleranceMultiplier=5):
    #     
    #     dataConfidence = util.bounds(0.5, dataConfidence, 0.99)
    #     #dataConfidence=0.5 checks for all values lower than 1st quartile or greater than 3rd quartile
    #     #dataConfidence<0.5 could cause weird behaviours
    #     #dataConfidence=1 would not check any value
    #     values = collector.sensor_data[1,:]
    #     nbsamples = values.size
    #     if nbsamples < 30: #not enough samples to check normality
    #         newcomment = "Normal distribution check on sensor_instance_id " + str(processor.sensor_instance_id)
    #         newcomment += " attempted but not enough samples to check for normal distribution " 
    #         newcomment += "(" + str(nbsamples) + " samples)"
    #         processor.raiseAlert(13, 0, newcomment)
    #         return
    #         
    #     index_upperLim = round( nbsamples*(1+dataConfidence)/2 ) -1 # (1+dataConfidence)/2 is the upper percentile above which the programm checks for outliers, -1 to ensure index_upperLim!=nbsamples
    #     index_lowerLim = round( nbsamples*(1-dataConfidence)/2 ) # (1-dataConfidence)/2 is the lower percentile under which the programm checks for outliers
    #     values.sort()
    #     valuesConfident = values[index_lowerLim:index_upperLim]
    #     stats, p = shapiro(valuesConfident) #checks the normality of the data
    #     if p < 0.05: #the data in the confidence interval is not normally distributed
    #         newcomment = "Normal distribution check on sensor_instance_id " + str(processor.sensor_instance_id)
    #         newcomment += " attempted but values do not follow normal distribution " 
    #         processor.raiseAlert(12, 0, newcomment)
    #         return
    #     
    #     mean = valuesConfident.mean()
    #     std = valuesConfident.std()
    #     #assumption: the whole dataset 'values' follows the same normal distribution than 'valuesConfident'
    #     threshold = 1/(toleranceMultiplier*nbsamples) #probability under which the value is considered an outlier
    #     outliers_low = 0 #number of outliers lower than lowerLim
    #     outliers_up = 0 #number of outliers greater than upperLim
    #     
    #     index = 0 #starts from the lowest index, increasing up to the confidence lower limit
    #     cumulative_prob = norm.cdf(values[index], loc=mean, scale=std)
    #     while( (cumulative_prob < threshold) and (index <= index_lowerLim) ):
    #         index += 1
    #         cumulative_prob = norm.cdf(values[index], loc=mean, scale=std)
    #         outliers_low += 1
    #     index = nbsamples-1 #strats from the highest index, decreasing up to the confidence upper limit
    #     cumulative_prob = norm.cdf(values[index], loc=mean, scale=std)
    #     while( (cumulative_prob > 1-threshold) and (index >= index_upperLim) ):
    #         index -= 1
    #         cumulative_prob = norm.cdf(values[index], loc=mean, scale=std)
    #         outliers_up += 1
    #     
    #     outliers = outliers_low + outliers_up
    #     if(outliers > 0):
    #         newcomment = "Normal distribution check on sensor_instance_id " + str(processor.sensor_instance_id)
    #         newcomment += " has detected " + str(outliers) + " suspicious outliers"
    #         newcomment += " (" + str(outliers_up) + " upper values, " + str(outliers_low) + " lower values)"
    #         processor.raiseAlert(11, 2, newcomment)