#====================================
#        Header, import modules
#====================================

from .. import util

import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

from sklearn.cluster import Birch
from sklearn.decomposition import PCA

#====================================
#        Class MLGroupClustering
#====================================

class MLGroupClustering:
    
    def __init__(self):
        return
        
    def getPoints(self, group):
        dimension = 2 #points are meant to be in a 2D space
        points = np.zeros( (len(group), dimension), dtype=float ) 
        
        for i, proc in enumerate(group):
            points[i, 0] = proc.collector.sensor_data[1,:].mean() #first coordinate: mean of the values
            points[i, 1] = proc.collector.sensor_data[1,:].std() #2nd coordinate: standard deviation
            #ideas for other coordinates: timedeltas between successive timestamps
        
        return points
    
    def checkMLGroupClustering(self, grp_proc, n_clusters=2):
        group = grp_proc.group
        filter = [grp_proc.location_filter, grp_proc.dimension_filter]
        X = self.getPoints(group)
        dimension = X.shape[1]
        
        if(dimension > 2):
            #proceed PCA
            pca = PCA(n_components = 2)
            X_reduced = pca.fit_transform(X)
            X = X_reduced #re-assignation to prevent case disjonction onwards
            
        model = Birch(threshold = 0.1, n_clusters = n_clusters)
        model.fit(X)
        yhat = model.predict(X)
        clusters = np.unique(yhat)
        
        #scatter plot
        fig, ax = plt.subplots()
        for cluster in clusters:
            row = np.where(yhat == cluster)
            ax.scatter( X[row, 0], X[row, 1] )
            for point in row[0]:
                ax.annotate( group[point].sensor_instance_id, (X[point, 0], X[point, 1]) ) #indicates the id of the sensor at the point coordinates
        
        title = "Group clustering suggestion for filter [" + str(filter[0]) + ", " + str(filter[1]) +"]"
        plt.title(title)
        path = "./monitoring_tool/checks/figures/clustering_" + grp_proc.database.properties["database"] + "_" + str(filter[0]) + "_" + str(filter[1])
        plt.savefig(path)
        plt.close()
        
            
        
        
    def runCheck(grp_proc):
        currCheck = MLGroupClustering()
        currCheck.checkMLGroupClustering(grp_proc, n_clusters=2)
        return