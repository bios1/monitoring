#====================================
#        Header, import modules
#====================================

from .. import constants
from .. import util

import numpy as np
import pandas as pd

from math import floor

#====================================
#        class TimestampsPeriod
#====================================

class TimestampsPeriod:
    
    def __init__(self):
        return
    
    def getPeriod(self, collector, threshold=0.9, tolerance=pd.Timedelta(3, unit="s")):
        #returns the period of the sensor
        #i.e. the time interval between threshold% of successive measures
        threshold = util.bounds(0.5, threshold, 0.99) #ensures threshold is in a given interval, threshold>0.5 is required to guarantee at most one return, 1 will return NaT if there is any error
        periods = [ [collector.sensor_data[0, 1] - collector.sensor_data[0, 0]], [1] ] #periods[0] are the list of periods, periods[1] are the count for each period
        nbsamples = collector.sensor_data[0,:].size
        for i in range(1, nbsamples-1): #begin at 1 because 1 period is already in the list
            newPeriod = collector.sensor_data[0, i+1] - collector.sensor_data[0, i]
            if newPeriod in periods[0]: #the period has already been found
                ind = periods[0].index(newPeriod) #index of the period in periods[0]
                periods[1][ind] += 1 #incrementing the period count
            else: #the period is new
                periods[0].append(newPeriod)
                periods[1].append(1)
        nbperiods = len(periods[0])

        periods.append( nbperiods*[None] ) #periods[2] are the count for each periods +/- tolerance
        for i in range(0, nbperiods): #i runs on periods[2]
            periods[2][i] = periods[1][i]
            for j in [x for x in range(0, nbperiods) if x != i]: #j != i
                if( abs(periods[0][i] - periods[0][j]) <= tolerance ):
                    periods[2][i] += periods[1][j]
        
        indmax2 = [] #list of the indexes with maximum period +/- tolerance count
        for i in range(0, nbperiods):
            if(periods[2][i] == max(periods[2])): #there is at least 1 element in indmax2
                indmax2.append(i)
        if(len(indmax2) > 1): #more than 1 maximum count on periods[2]
            # then compares periods[1]
            indmax1 = [] #list of the indexes with maximum period count
            periods21 = [periods[1][index] for index in indmax2]
            for i in indmax2:
                if(periods[1][i] == max(periods21)):
                    indmax1.append(i)
            if(len(indmax1) > 1): #more than 1 maximum count on periods[1] among the indexes in indmax2, period ambiguous
                return pd.NaT
            else:
                indmax2 = indmax1[0] #indmax2 is now an int
        else:
            indmax2 = indmax2[0]
            
        if( periods[2][indmax2]/nbsamples >= threshold ):
            return periods[0][indmax2]
        return pd.NaT #returns NaT if no clear period has been found
    
    
    def getInterruptionDuration(self, interBegin, interEnd, boundLow=pd.NaT, boundHigh=pd.NaT):
        if(pd.isnull(boundLow)):
            boundLow = interBegin[0]
        if(pd.isnull(boundHigh)):
            boundHigh = interEnd[-1]
        if(boundHigh < boundLow):
            raise ValueError("In getInteruptionDuration, bounds are interverted")
        
        total_duration = pd.Timedelta(0)
        sublistBegin = []
        sublistEnd = []
        for i in range(0, len(interBegin)): #interBegin and interEnd have the same number of elements
            if( interBegin[i] > boundLow and interBegin[i] < boundHigh ):
                sublistBegin.append(interBegin[i])
            if( interEnd[i] > boundLow and interEnd[i] < boundHigh ):
                sublistEnd.append(interEnd[i])
            
        if( len(sublistBegin) == len(sublistEnd) ): #all interuptions began and ended within the bounds
            for i in range(0, len(sublistBegin)):
                total_duration += sublistEnd[i] - sublistBegin[i]
        elif( len(sublistBegin) == len(sublistEnd)+1 ): #the last interruption began before boundHigh but ended after boundHigh
            for i in range(0, len(sublistEnd)):
                total_duration += sublistEnd[i] - sublistBegin[i]
            total_duration += boundHigh - sublistBegin[-1]
        elif( len(sublistBegin) == len(sublistEnd)-1 ): #the first interruption began before boundLow but ended after boundLow
            total_duration += sublistEnd[0] - boundLow
            for i in range(0, len(sublistBegin)):
                total_duration += sublistEnd[i+1] - sublistBegin[i]
                
        return total_duration
    
    
    def checkTimestampsPeriod(self, processor, collector, slicing_unit="d"):
        #does the number of values fit the period of the sensor?
        #did the sensor go offline?
        period = self.getPeriod(collector)
        if(pd.isnull(period)): #the sensor doesnt have a clear period
            return
        
        actual = collector.sensor_data[1,:].size #number of values
        
        #check for activity interruption
        interruption_begin = []
        interruption_end = []
        total_interruption = pd.Timedelta(0)
        for i in range(0, actual-1):
            curr_period = collector.sensor_data[0,i+1] - collector.sensor_data[0,i]
            if(curr_period > 10*period): #if sensor went offline
                interruption_begin.append(collector.sensor_data[0,i])
                interruption_end.append(collector.sensor_data[0,i+1])
        for i in range(0, len(interruption_begin)):
            interruption_duration = interruption_end[i] - interruption_begin[i]
            total_interruption += interruption_duration
        #alerts
        if(len(interruption_begin) > 0):
            newcomment = "sensor_instance_id " + str(processor.sensor_instance_id)
            newcomment += " probably went off " + str(len(interruption_begin))
            newcomment += " time(s) between " + str(collector.sensor_data[0,0])
            newcomment += " and " + str(collector.sensor_data[0,-1]) + "."
            newcomment += " Total estimated offline time: " + str(total_interruption)
            processor.raiseAlert( 22, 1, newcomment)
        
        processed = 0
        while(processed != actual):
            time_low = collector.sensor_data[0,processed].floor(freq = slicing_unit)
            time_high = time_low + pd.Timedelta(1, unit = slicing_unit)
            sublist = [] #sublist = timestamps between time_low and time_high
            while(collector.sensor_data[0,processed] < time_high): #ok because timestamps are sorted
                sublist.append( collector.sensor_data[0,processed] )
                processed += 1
            interruption_duration = self.getInterruptionDuration(interruption_begin, interruption_end, time_low, time_high) #duration of interruption between time low and time high
            actual = len(sublist) #actual number of values between bounds
            expected = (time_high - time_low - interruption_duration)/period
            expected = floor(expected)
            
            # raise alerts
            fmt = '%Y-%m-%d %H:%M:%S'
            str_low = time_low.strftime(fmt)
            str_high = time_high.strftime(fmt)
            if(actual < expected):
                newcomment = "sensor_instance_id " + str(processor.sensor_instance_id)
                newcomment += " has not enough values between "
                newcomment += str_low + " and " + str_high
                newcomment += ": has " + str(actual)
                newcomment += " values but expected " + str(expected) 
                newcomment += " (period=" + str(period) + ")"
                processor.raiseAlert( 20, 1, newcomment, time_low, time_high)
            elif(actual > expected):
                newcomment = "sensor_instance_id " + str(processor.sensor_instance_id)
                newcomment += " has too many values between "
                newcomment += str_low + " and " + str_high
                newcomment += ": has " + str(actual)
                newcomment += " values but expected " + str(expected) 
                newcomment += " (period=" + str(period) + ")"
                processor.raiseAlert( 21, 1, newcomment, time_low, time_high)
            
    def runCheck(processor, collector):
        currCheck = TimestampsPeriod()
        currCheck.checkTimestampsPeriod(processor, collector)