# Timestamps validity check   

## Imports required

This file requires the import of `constants`, `util`, `binding` and `collecting` as it reuses assets from these files, notably those related to timestamps and timezones.  
The standard libraries `numpy` and `pandas` are also required for handling data.  

## How does it work?

Located in the file `timestampsValidity.py`.  
This check looks at the `collector`'s timestamps for missing values (`NaT`, "not a timestamp") or invalid timestamps (e.g., in the future).  
If there is any, can raise two types of alert (one for missing timestamps, another for invalid timestamps), each detailling the number of samples affected.  
Said samples associated with missing/invalid timestamps are deleted from the `collector` for further processing (not deleted from the database).  

## Check information

Check ID: 1  
Alert types that can be raised:  
| Alert.type    | Raised by check    | Error         |
| :----: | :----: |-------------------------------|
| 10 	 | 1      | The sensor has undefined timestamps      |
| 11 	 | 1      | The sensor has invalid timestamps (e.g., in the future)     |
