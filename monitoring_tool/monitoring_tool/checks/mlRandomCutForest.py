#====================================
#        Header, import modules
#====================================

from .. import constants
from .. import util

import numpy as np
import pandas as pd

import rrcf

import matplotlib.pyplot as plt

#====================================
#        class MLRandomCutForest
#====================================

class MLRandomCutForest:
    
    def __init__(self):
        return
        
    def checkMLRandomCutForest(self, processor, collector):
        #timestamps_seconds = timestamps in second from the first sample
        ref = collector.sensor_data[0,0] #first timestamp
        timestamps_seconds = []
        for ts in collector.sensor_data[0,:]:
            dt = ts - ref #pandas timedelta 
            timestamps_seconds.append( dt.total_seconds() ) #append timesdelta as float in seconds
        
        #build the point sets
        nbsamples = collector.sensor_data[0,:].size
        X = np.zeros( (2, nbsamples) )
        for i in range(0, nbsamples):
            X[0, i] = timestamps_seconds[i]
            X[1, i] = collector.sensor_data[1,i]
            
        #detrended point set
        Xdetrend = np.zeros( (2, nbsamples-1) )
        for i in range(0, nbsamples-1):
            Xdetrend[0, i] = X[0, i+1]
            Xdetrend[1, i] = X[1, i+1] - X[1, i]
        
        
        #parameters
        l_nbtrees = [100] #number of trees in a forest
        l_tree_size = [256] #number of leaves in a tree
        l_shingle_size = [20, 40] #number of consecutive samples in a leave
        list_configs = [ l_nbtrees, l_tree_size, l_shingle_size ] #lists of the lists of possible parameter values
        cart_prod = [(a,b,c) for a in list_configs[0] for b in list_configs[1] for c in list_configs[2]] #cartesian product = all the configurations possible
        
        #plot initialization
        fig, ax1 = plt.subplots(figsize=(10, 5))
        ax1.set_ylabel('Data values', color='blue', size=14)
        ax1.set_xlabel('Sample index', color='black', size=14)
        ax1.plot(X[1,:], label='values', color='blue')
        ax1.tick_params(axis='y', labelcolor='blue', labelsize=12)
        figtitle = "Random Cut forest result for sensor instance " + str(collector.sensor_instance_id) + " (dimension " + str(collector.sensor_dimension) + ")"
        plt.title(figtitle)
        ax2 = ax1.twinx()
        ax2.set_ylabel('CoDisp values', color='black', size=14)
        
        for config in cart_prod:
            
            #parameters for the current config
            nbtrees = config[0]
            tree_size = config[1]
            shingle_size = config[2]
        
            # Create a forest of empty trees
            forest = []
            for _ in range(nbtrees):
                tree = rrcf.RCTree()
                forest.append(tree)
                
            points = rrcf.shingle(X[1,:], size=shingle_size) # Use the "shingle" generator to create rolling window
            avg_codisp = {} # Create a dict to store anomaly smonitoring_tool of each point
            
            # For each shingle...
            for index, point in enumerate(points):
                index += shingle_size//2 #adds half the shingle size to the index so it is centered, 
                # For each tree in the forest...
                for tree in forest:
                    # If tree is above permitted size, drop the oldest point (FIFO)
                    if len(tree.leaves) > tree_size:
                        tree.forget_point(index - tree_size)
                    # Insert the new point into the tree
                    tree.insert_point(point, index=index)
                    # Compute codisp on the new point and take the average among all trees
                    if not index in avg_codisp:
                        avg_codisp[index] = 0
                    avg_codisp[index] += tree.codisp(index)/nbtrees
            
            curr_label = "avg_codisp, n=" + str(nbtrees) + ", t=" + str(tree_size) + ", s=" + str(shingle_size)
            ax2.plot(pd.Series(avg_codisp).sort_index(), label=curr_label)
            
            points_detrend = rrcf.shingle(Xdetrend[1,:], size=shingle_size)
            avg_codisp_detrend = {}
            forest = []
            for _ in range(nbtrees):
                tree = rrcf.RCTree()
                forest.append(tree)
            for index, point in enumerate(points_detrend):
                index += shingle_size//2 #adds half the shingle size to the index so it is centered, 
                # For each tree in the forest...
                for tree in forest:
                    # If tree is above permitted size, drop the oldest point (FIFO)
                    if len(tree.leaves) > tree_size:
                        tree.forget_point(index - tree_size)
                    # Insert the new point into the tree
                    tree.insert_point(point, index=index)
                    # Compute codisp on the new point and take the average among all trees
                    if not index in avg_codisp_detrend:
                        avg_codisp_detrend[index] = 0
                    avg_codisp_detrend[index] += tree.codisp(index)/nbtrees
            
            curr_label += " (detrended)"
            ax1.plot(Xdetrend[1,:], label='detrended values', color='blue', linestyle='--')
            ax2.plot(pd.Series(avg_codisp_detrend).sort_index(), label=curr_label, linestyle='--')
            
        
        #save figure, then close it
        plt.legend()
        path = "./monitoring_tool/checks/figures/rrcf_" + dbprop["database"] + "_" + str(collector.sensor_instance_id)
        plt.savefig(path)
        plt.close()

    def runCheck(processor, collector):
        currCheck = MLRandomCutForest()
        currCheck.checkMLRandomCutForest(processor, collector)