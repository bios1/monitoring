# Values Normal distribution check

## Imports required

This file requires the import of `constants`, `util`, `binding` and `collecting` as it reuses assets from these files, notably those related to timestamps and timezones.  
The standard libraries `numpy` and `pandas` are also required for handling data.  
Furthermore, in order to perform task specific to analysis of normal distribution, this module requires the methods `shapiro` (statistical test) and `norm` from `scipy.stats`.  

## How does it work?

Located in the file `valuesNormalDist.py`.  
This check requires additionnal imports, those being `scipy.stats.norm` and `scipy.stats.shapiro`.  
This check uses additionnal arguments:  
 + `toleranceMultiplier`, default value 5, is a parameter that allows flexibility on when to raise alerts. Its exact interpretation (and default value) may vary between methods, but it is intended that increasing `toleranceMultiplier` will increase the tolerance for values not to be considered outliers, reducing the number of alert and (hopefully) increasing the precision (i.e. reducing the number of false alerts).  

This method will check the values for outliers using statistics on normal distribution. This check is asynchronous.  
In detail:  
 + `values` is the sorted array of values, `nbsamples` is the number of samples

If the number of samples is below 30, the check exits since there are not enough values to confidently assume the values follow a normal distribution.  
The programm then uses [Shapiro's test](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.shapiro.html) to verify if `values` follows a normal distribution.  
If not, the programm raises an alert of level 0, stating that normal distribution test has been attempted on non-normally distributed data and exits the method before further processing.  
If `values` is normally distributed, then:
 + `mean` and `std` are respectively the mean and standard deviation of the distribution.  
 + `threshold = 1/(toleranceMultiplier*nbsample)` is the probability under which the value is considered an outlier, i.e. "the samples should be `toleranceMultiplier` times more numerous for a value with this probability to be likely to happen".  

The programm counts the outliers:
 + a lower value is an outlier if the cumulative probability `P(X < value)` is lower than `threshold`.  
 + an upper value is an outlier if the cumulative probability `P(X < value)` is greater than `1 - threshold` (equivalent to `P(X > value) < threshold`).  

For a visual explanation of how outliers are found, please look at this image from [Towards Data Science](https://towardsdatascience.com/understanding-boxplots-5e2df7bcbd51):  

![alt text](./figures/doc_normaldist.png "Outliers in a normal distribution")  

Please note that this picture does not acknowledge for the use of the tolerance multiplier used in the programm, and that the programm does not use the number of sigmas but a threshold inversely proportional to the number of samples to workout if a value is an outlier.  

<!-- former version
This check requires additionnal imports, those being `scipy.stats.norm` and `scipy.stats.shapiro`.  
This check uses additionnal arguments:  
 + `dataConfidence`, default value 0.9, is the proportion of values that is assumed to be safe. When checking for outliers, the inner values (those closer to the median) are considered safe.  
 + `toleranceMultiplier`, default value 5, is a parameter that allows flexibility on when to raise alerts. Its exact interpretation (and default value) may vary between methods, but it is intended that increasing `toleranceMultiplier` will increase the tolerance for values not to be considered outliers, reducing the number of alert and (hopefully) increasing the precision (i.e. reducing the number of false alerts).  

This method will check the `1-dataConfidence`% most extreme values (equally divided between upper and lower values) for outliers using statistics on normal distribution. This check is asynchronous.  
In detail:  
 + `values` is the sorted array of values, `nbsamples` is the number of samples
 + `valuesConfident` is the array of values that are assumed safe, i.e. it contains all values except the `1-dataConfidence`% most extreme ones.
The programm then uses [Shapiro's test](https://docs.scipy.org/doc/scipy/reference/generated/scipy.stats.shapiro.html) to verify if `valuesConfident` follows a normal distribution.  
If not (or if there is less than 30 samples, which is not enough to be confident in following a normal distribution), the programm raises an alert of level 0, stating that normal distribution test has been attempted on non-normally distributed data and exits the method before further processing.  
If `valuesConfident` is normally distributed, then:
 + the programm makes the assumption that `values` actually follows the same normal distribution as `valuesConfident`.  
 + `mean` and `std` are respectively the mean and standard deviation of the distribution.  
 + `threshold = 1/(toleranceMultiplier*nbsample)` is the probability under which the value is considered an outlier, i.e. "the samples should be `toleranceMultiplier` times more numerous for a value with this probability to be likely to happen".  

The programm counts the outliers (both lower and upper ones) by comparing the cumulative probability of being lower than the values to the threshold.  
-->

## Check information

Check ID: 4  
Alert types that can be raised:  
| Alert.type    | Raised by check    | Error         |
| :----: | :----: |-------------------------------|
| 40 	 | 4      | Normal distribution test on the sensor has detected suspicious outliers |
| 41 	 | 4      | Normal distribution test attempted on values that do not follow a normal distribution |
| 42 	 | 4      | Normal distribution test attempted but not enough samples to check normality |