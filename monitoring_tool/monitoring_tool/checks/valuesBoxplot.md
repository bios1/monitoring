# Values boxplot check

## Imports required

This file requires the import of `constants`, `util`, `binding` and `collecting` as it reuses assets from these files, notably those related to timestamps and timezones.  
The standard libraries `numpy` and `pandas` are also required for handling data.  

## How does it work?

Located in the file `valuesBoxplot.py`.  
This check uses additionnal arguments:  
 + `toleranceMultiplier`, default value 5, is a parameter that allows flexibility on when to raise alerts. Its exact interpretation (and default value) may vary between methods, but it is intended that increasing `toleranceMultiplier` will increase the tolerance for values not to be considered outliers, reducing the number of alert and (hopefully) increasing the precision (i.e. reducing the number of false alerts).  

This method will check the most extreme values for outliers using boxplots. This check is asynchronous.  
In detail:  
 + `values` is the sorted array of values, `nbsamples` is the number of samples.  
 + `median`, `quartile1` and `quartile3` are respectively the median, first quartile and third quartile.  
 + `iqr` is the interquartile range (i.e., `quartile3 - quartile1`) and `tolerance` is defined as `tolerance = 1.5*iqr*toleranceMultiplier`  

The programm will compare the distances, on one hand between the value and the extreme limit associated, and on the other hand between the median and the quartile, and acknowledges for `toleranceMultiplier`.  
 + for lower extreme values, the value is declared an outlier if: `value < quartile1 - tolerance`  
 + for upper extreme values, the value is declared an outlier if: `value > quartile3 + tolerance`  

For a visual explanation of how outliers are found, please look at this image from [Towards Data Science](https://towardsdatascience.com/understanding-boxplots-5e2df7bcbd51):  

![alt text](./figures/doc_boxplot.png "Outliers in a boxplot")  

Please note that this picture does not acknowledge for the use of the tolerance multiplier used in the programm.  

<!-- former version
This check uses additionnal arguments:  
 + `dataConfidence`, default value 0.9, is the proportion of values that is assumed to be safe. When checking for outliers, the inner values (those closer to the median) are considered safe.  
 + `toleranceMultiplier`, default value 5, is a parameter that allows flexibility on when to raise alerts. Its exact interpretation (and default value) may vary between methods, but it is intended that increasing `toleranceMultiplier` will increase the tolerance for values not to be considered outliers, reducing the number of alert and (hopefully) increasing the precision (i.e. reducing the number of false alerts).  

This method will check the `1-dataConfidence`% most extreme values (equally divided between upper and lower values) for outliers using boxplots. This check is asynchronous.  
In detail:  
 + `values` is the sorted array of values, `nbsamples` is the number of samples.  
 + `lowerLim` is the value under which the programm checks for outliers. Its percentile is `(1-dataConfidence)/2`.  
 + `upperLim` is the value above which the programm checks for outliers. Its percentile is `(1+dataConfidence)/2`.  
 + `median`, `quartile1` and `quartile3` are respectively the median, first quartile and third quartile.  

The programm will compare the distances, on one hand between the value and the extreme limit associated, and on the other hand between the median and the quartile, and acknowledges for `toleranceMultiplier`.  
 + for lower extreme values, the value is declared an outlier if: `lowerLim - value > toleranceMultiplier*(median-quartile1)`  
 + for upper extreme values, the value is declared an outlier if: `value - upperLim > toleranceMultiplier*(quartile3-median)`  
-->

If any outlier is detected, an alert is raised, detailing the number of outliers by lower or higher value.    

## Check information

Check ID: 3  
Alert types that can be raised:  
| Alert.type    | Raised by check    | Error         |
| :----: | :----: |-------------------------------|
| 30 	 | 3      | Boxplot test on the sensor has detected suspicious outliers |