# Values derivatives check

## Imports required

This file requires the import of `constants`, `util`, `binding` and `collecting` as it reuses assets from these files, notably those related to timestamps and timezones.  
The standard libraries `numpy` and `pandas` are also required for handling data.  

## How does it work?

Located in the file `valuesDerivatives.py`.  
This check uses additionnal arguments:  
 + `toleranceMultiplier`, default value 5, is a parameter that allows flexibility on when to raise alerts. Its exact interpretation (and default value) may vary between methods, but it is intended that increasing `toleranceMultiplier` will increase the tolerance for values not to be considered outliers, reducing the number of alert and (hopefully) increasing the precision (i.e. reducing the number of false alerts).  

This check looks for anomalies in derivatives of the sensors' values. This check is synchronous.  
An anomaly is defined as follow:
 + the derivative changes sign
 + the derivative's absolute value is `toleranceMultiplier` times greater than the median of absolute derivatives.  

Two other checks have been defined but are not used, for they are too sensitive: a derivative sign check and an absolute derivative check, but not combined.  

## Check information

Check ID: 5  
Alert types that can be raised:  
| Alert.type    | Raised by check    | Error         |
| :----: | :----: |-------------------------------|
| 50 	 | 5      | Derivative check detected values are supposed to increase, but some derivatives are negative |
| 51 	 | 5      | Derivative check detected values are supposed to decrease, but some derivatives are positive |
| 52 	 | 5      | Derivative check detected some derivatives have a suspiciously high absolute value |
| 53 	 | 5      | Derivative check detected values for which derivative changes sign and has high absolute value |