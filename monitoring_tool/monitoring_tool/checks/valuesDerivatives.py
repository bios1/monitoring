#====================================
#        Header, import modules
#====================================

from .. import constants
from .. import util

import numpy as np
import pandas as pd

#====================================
#        class ValuesDerivatives
#====================================

class ValuesDerivatives:
    
    def __init__(self):
        return
        
    def checkSign(self, processor, derivatives):
        nbderiv = derivatives.size
        
        if( sum(derivatives > 0)/nbderiv > 0.95 ): #values are supposed to increase
            if( sum(derivatives < 0) > 0 ): #but some derivatives are <0
                newcomment = "Derivative check on sensor_instance_id " + str(processor.sensor_instance_id)
                newcomment += " detected the values are supposed to increase, but "
                newcomment += str(sum(derivatives < 0)) + " derivatives are negative."
                processor.raiseAlert( 50, 0, newcomment)
                
        elif( sum(derivatives < 0)/nbderiv > 0.95 ): #values are supposed to decrease
            if( sum(derivatives > 0) > 0 ): #but some derivatives are >0
                newcomment = "Derivative check on sensor_instance_id " + str(processor.sensor_instance_id)
                newcomment += " detected the values are supposed to decrease, but "
                newcomment += str(sum(derivatives > 0)) + " derivatives are positive."
                processor.raiseAlert( 51, 0, newcomment)
        
    
    def checkAbsoluteDerivative(self, processor, derivatives, toleranceMultiplier):
        nbderiv = derivatives.size
        absdev = abs(derivatives)
        absdev.sort()
        median = absdev[nbderiv//2]
        if( sum(absdev > toleranceMultiplier*median) > 0 ):
            newcomment = "Derivative check on sensor_instance_id " + str(processor.sensor_instance_id)
            newcomment += " detected " + str(sum(absdev > toleranceMultiplier*median))
            newcomment += " derivatives that are " + str(toleranceMultiplier) + " times greater in absolute value than "
            newcomment += " the median value (" + str(median) + ")"
            processor.raiseAlert( 52, 0, newcomment)
            
    def checkDerivativeShift(self, processor, derivatives, toleranceMultiplier):
        nbderiv = derivatives.size
        
        #indexes for which the absolute value is high
        absdev = abs(derivatives)
        absdev.sort()
        median = absdev[nbderiv//2]
        indexes_abshigh = (absdev > toleranceMultiplier*median)
        
        #indexes for which the derivatives changes sign
        indexes_signshift = nbderiv*[False]
        for i in range(1, nbderiv):
            if( derivatives[i-1]*derivatives[i] < 0 ):
                indexes_signshift[i] = True
        indexes_signshitt = np.array(indexes_signshift)
        
        #anomaly = high absolute value and sign shift
        indexes_anomalies = indexes_abshigh * indexes_signshift
        if( sum(indexes_anomalies) > 0 ):
            newcomment = "Derivative check on sensor_instance_id " + str(processor.sensor_instance_id)
            newcomment += " detected " + str(sum(indexes_anomalies))
            newcomment += " potential anomalies (high absolute value and sign shift)"
            newcomment += " out of " + str(nbderiv) + " values."
            processor.raiseAlert( 53, 1, newcomment)
        
            
    
    def checkValuesDerivatives(self, processor, collector, toleranceMultiplier=5):
        
        timestamps = collector.sensor_data[0, :]
        values = collector.sensor_data[1, :]
        derivatives = np.zeros( values.size -1 )
        nbderiv = derivatives.size
        for i in range(0, nbderiv): #computes the derivatives
            delta_time = timestamps[i+1] - timestamps[i]
            delta_time = delta_time.total_seconds() #conversion in number of seconds to have a float and allow for division
            delta_value = values[i+1] - values[i]
            derivatives[i] = delta_value/delta_time
            
        # self.checkSign(processor, conn, dbprop, derivatives) #too sensitive
        # self.checkAbsoluteDerivative(processor, conn, dbprop, derivatives, toleranceMultiplier) # too sensitive
        self.checkDerivativeShift(processor, derivatives, toleranceMultiplier)
        
        
    def runCheck(processor, collector):
        currCheck = ValuesDerivatives()
        currCheck.checkValuesDerivatives(processor, collector)