#====================================
#        Header, import modules
#====================================

from .. import constants
from .. import util

import numpy as np
import pandas as pd

#====================================
#        class ValuesBoxplot
#====================================

class ValuesBoxplot:

    def __init__(self):
        return
    
    def checkValuesBoxplot(self, processor, collector, toleranceMultiplier=5):
        values = collector.sensor_data[1,:]
        nbsamples = values.size
        index_median = round(nbsamples/2)
        index_quartile1 = round(nbsamples/4)
        index_quartile3 = round(3*nbsamples/4)
        values.sort()
        median = values[index_median]
        quartile1 = values[index_quartile1]
        quartile3 = values[index_quartile3]
        iqr = quartile3 - quartile1 #inter quartile range
        tolerance = 1.5*toleranceMultiplier*iqr
        
        outliers_low = 0 #number of outliers lower than lowerLim
        outliers_up = 0 #number of outliers greater than upperLim
        
        i = 0 #checks the lower values for outliers
        while( values[i] < quartile1 - tolerance ):
            outliers_low += 1
            i += 1
            
        i = nbsamples -1 #checks the higher values for outliers
        while( values[i] > quartile3 + tolerance):
            outliers_up += 1
            i -= 1
        
        outliers = outliers_low + outliers_up
        if(outliers > 0):
            newcomment = "Boxplot check on sensor_instance_id " + str(processor.sensor_instance_id)
            newcomment += " has detected " + str(outliers) + " suspicious outliers"
            newcomment += " (" + str(outliers_up) + " upper values, " + str(outliers_low) + " lower values)"
            processor.raiseAlert( 30, 2, newcomment)
            
    def runCheck(processor, collector):
        currCheck = ValuesBoxplot()
        currCheck.checkValuesBoxplot(processor, collector)
        
        
    ##former implementation below
    # def checkValuesBoxplot(self, processor, collector, dataConfidence=0.90, toleranceMultiplier=10):
    #     dataConfidence = util.bounds(0.5, dataConfidence, 0.99)
    #     #dataConfidence=0.5 checks for all values lower than 1st quartile or greater than 3rd quartile
    #     #dataConfidence<0.5 could cause weird behaviours
    #     #dataConfidence=1 would not check any value
    #     values = collector.sensor_data[1,:]
    #     nbsamples = values.size
    #     index_median = round(nbsamples/2)
    #     index_quartile1 = round(nbsamples/4)
    #     index_quartile3 = round(3*nbsamples/4)
    #     index_upperLim = round( nbsamples*(1+dataConfidence)/2 ) -1 # (1+dataConfidence)/2 is the upper percentile above which the programm checks for outliers, -1 to ensure index_upperLim!=nbsamples
    #     index_lowerLim = round( nbsamples*(1-dataConfidence)/2 ) # (1-dataConfidence)/2 is the lower percentile under which the programm checks for outliers
    #     values.sort()
    #     median = values[index_median]
    #     quartile1 = values[index_quartile1]
    #     quartile3 = values[index_quartile3]
    #     upperLim = values[index_upperLim]
    #     lowerLim = values[index_lowerLim]
    #     
    #     outliers_low = 0 #number of outliers lower than lowerLim
    #     outliers_up = 0 #number of outliers greater than upperLim
    #     for i in range(0, index_lowerLim): #checks for outliers under the lowerLim
    #         if( (lowerLim - values[i]) > toleranceMultiplier*(median-quartile1) ):
    #             outliers_low += 1
    #     for i in range(index_upperLim, nbsamples): #checks for outliers above the upperLim
    #         if( (values[i] - upperLim) > toleranceMultiplier*(quartile3-median) ):
    #             outliers_up += 1
    #     
    #     outliers = outliers_low + outliers_up
    #     if(outliers > 0):
    #         newcomment = "Boxplot check on sensor_instance_id " + str(processor.sensor_instance_id)
    #         newcomment += " has detected " + str(outliers) + " suspicious outliers"
    #         newcomment += " (" + str(outliers_up) + " upper values, " + str(outliers_low) + " lower values)"
    #         processor.raiseAlert(10, 2, newcomment)