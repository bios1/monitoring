#!/usr/bin/env python3
# -*- coding: utf-8

from docopt import docopt

usage ="""
Monitoring tool for IoT devices

Usage:
  monitoring_tool <database_name>

Options::
  database_name     Name of the database. It should correspond to the name of the configuration file in ~/.config/monitoring_tool For example, weather if the configration file is weather.txt
"""

def main():
    """Main loop of the monitoring service
    
    This function is the loop that is executed at each monitoring session.
    It handles the retrieval of local files, the connection to the database,
    the data processing and the user feedback.
    """
    import monitoring_tool.constants
    from monitoring_tool import constants
    from monitoring_tool import util
    from monitoring_tool import collecting
    from monitoring_tool import processing
    from monitoring_tool.database import Database
    from monitoring_tool.sensors import Sensors

    import pandas as pd
    import numpy as np

    from pika import PlainCredentials

    import os

    args = docopt(usage)
    config_file = "{}/.config/monitoring_tool/".format(os.environ['HOME']) + args['<database_name>'] + ".txt"

    constants.setTimezones(True)
    db = Database(config_file)

    try:
        file = open("{}/.config/monitoring_tool/rabbitmq.credentials".format(os.environ['HOME']))
    except Exception as err:
        print(err)
        exit()
    
    (hostname, port) = tuple(file.readline().split(':', 2))
    username = file.readline().strip()
    password = file.readline().strip()
    file.close()

    credentials = PlainCredentials(username, password)

    exchange_name = db.properties["database"] + "-check-alert"

    sensors = Sensors(
        database=db,
        rabbitmq_hostname=hostname,
        rabbitmq_credentials=credentials,
        rabbitmq_port=port,
        rabbitmq_exchange_name=exchange_name)
    
    time_start = pd.to_datetime("today").normalize()
    time_stop = pd.to_datetime("now", utc=True).tz_convert(constants.glob_str_tz_dest)
    sensors.run_tests_instances()
    sensors.run_tests_groups()
    db.disconnect()

if __name__ == "__main__":
    main()
