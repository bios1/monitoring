#====================================
#        Header, import modules
#====================================

from . import constants
from . import util
from . import database

import numpy as np
import pandas as pd

#====================================
#       class Collector
#====================================

class Collector:
    """Handles sensor identification and data retrieval.

    A Collector object is bound to one sensor. It is an interface with the
    database to collect information about the sensor and the values of its
    measurements.

    Args:
        database (:py:class:`Database`) : Database in which the collector
                                          gathers data.
        targetID (int) : ID of the sensor in the database.
        version (int) : Version of the database in which the data must be
                        collected. If None (default), the version is set to the
                        latest.
        dataRetrieval (bool) : If set to True, the data will be gather during
                               instanciation. Otherwise, :py:meth:`getData`
                               must be called later.
                               Default : True

    Attributes:
        database (:py:class:`Database`) : Database in which the collector gathers
                                          data.
        sensor_instance_id (int) : Unique ID of the sensor.
        sensor_dimension (int) : ID of the physical quantity measured by the
                                 sensor.
        sensor_location (int) : ID of the location of the sensor.
        sensor_data (:obj:`ndarray`) : Two dimensional array containing the
                                       pandas type Timestamp of the measurement
                                       and the values. Timestamps are accessed
                                       with sensor_data[0,:] and values with
                                       sensor_data[1,;].
        identified (bool) : Set to False if it is impossible to identify the
                            sensor (e.g ID not found, multiple IDs found...)
        instance_checks_dict (dict) : Dictionary containing the methods 
                                      corresponding to the different checks that
                                      can be executed. Keys are the checks IDs.


    """

    def __init__(self, database, targetID, version=None, dataRetrieval=True):
        self.database = database
        targetID = int(targetID)
        self.identified = False
        self.sensor_instance_id = -1
        self.sensor_dimension = -1
        self.sensor_location = -1
        self.sensor_data = np.array([])
        self.getInfo(targetID)
        self.instance_checks_dict = self.query_instance_checks()
        if( self.identified ):
            self.getLocation()
            if(dataRetrieval): #retrieves the data at object instanciation. If not, getData must be called later
                self.getData(sinceVersionRelease=version)
    
    def getInfo(self, targetID):
        """Query information about the sensor and save them in the object attributes.

        Args:
            targetID (int): ID of the sensor in the database.

        Note:
            This method is called when the object is initialized. You should not
            have to call it.

        """
        query = ("SELECT {t2}.{c2}, {t1}.{c1} FROM {t2}"
                " JOIN {t1} ON {t2}.{fk21} = {t1}.{pk1}"
                " WHERE {t2}.{c2} = %s")
        query = query.format(t1 = self.database.properties["sensorDimension_table"],
                             t2 = self.database.properties["sensorInstance_table"],
                             c1 = self.database.properties["sensorDimension_id"],
                             c2 = self.database.properties["sensorInstance_id"],
                           fk21 = self.database.properties["sensorInstance_fk_sensorDimension"],
                            pk1 = self.database.properties["sensorDimension_pk"])
        param = (targetID, )
        if(self.database.properties["version_sensitive"] == '1'):
            version = self.database.version
            query += " AND {t2}.{c3} = %s"
            param = (targetID, version)
            query = query.format(t2 = self.database.properties["sensorInstance_table"],
                                c3 = self.database.properties["sensorInstance_version"])
        temp = pd.read_sql_query(query, self.database.conn, params=param)
        if(len(temp.index) != 1): #sensor identification is either failed (no match) or ambiguous (multiple matches)
            self.identified = False
            self.sensor_instance_id = -1
            self.sensor_dimension = -1
            self.sensor_location = -1
            self.sensor_data = np.zeros(shape=(2, 0), dtype=float) #empty array for both values and timestamps
        else: #the sensor is well identified
            self.identified = True
            temp = temp.to_numpy()
            self.sensor_instance_id = temp[0][0].item()
            self.sensor_dimension = temp[0][1].item()
        
    def getLocation(self):
        """Query the location of the sensor and save it in :py:attr:`sensor_location`.

        If it is impossible to get the location of the sensor,
        :py:attr:`sensor_location` is set to -1.
        """
        if(not self.identified):
            #set location variable to -1, as it is a value that does not appear in the db
            self.sensor_location = -1
            return
        query = ("SELECT DISTINCT( {t2}.{c2} ) FROM "
                "{t2} JOIN {t1} ON {t2}.{pk2}={t1}.{fk12} "
                "WHERE {t1}.{c1} = %s ")
        query = query.format(t1 = self.database.properties["sensorInstance_table"],
                             t2 = self.database.properties["location_table"],
                             c1 = self.database.properties["sensorInstance_id"],
                             c2 = self.database.properties["location_id"],
                             fk12 = self.database.properties["sensorInstance_fk_location"],
                             pk2 = self.database.properties["location_pk"])
        param = (self.sensor_instance_id, )
        
        if(self.database.properties["version_sensitive"]=='1'):
            version = self.database.version
            query += " AND {t1}.{c4} = %s "
            query = query.format(t1 = self.database.properties["sensorInstance_table"],
                                c4 = self.database.properties["sensorInstance_version"])
            param = (self.sensor_instance_id, version)
    
        temp = pd.read_sql_query(query, self.database.conn, params = param)
        if(len(temp.index) == 1): #the location of the sensor is exactly identified
            temp = temp.to_numpy()
            self.sensor_location = temp[0][0].item()
        else: #the location of the sensor is either non-existent (no output) or ambiguous (output with 2 rows or more)
            #set location variable to -1, as it is a value that does not appear in the db
            self.sensor_location = -1
        
    def getData(self, sinceVersionRelease=None):
        """Query the data of the measurements made by the sensor and saves it in
        :py:attr:`sensor_data`.

        Args:
            sinceVersionRelease (int, optional): depreciated. Defaults to None.
        """
        if(not self.identified):
            return
        query = ("SELECT {c1}, {c2} FROM {t2}"
                " JOIN {t1} ON {t2}.{fk21} = {t1}.{pk1}"
                " WHERE {t1}.{c3} = %s")
        param = (self.sensor_instance_id, )
        if(self.database.properties["version_sensitive"]=='1'):
            date = self.database.version_date
            query += " AND {t2}.{c1} > %s"
            param = (self.sensor_instance_id, date)
        query += " ORDER BY {t2}.{c1} ASC "
        query = query.format(c1 = self.database.properties["sensorValues_timestamp"],
                             c2 = self.database.properties["sensorValues_value"],
                             t1 = self.database.properties["sensorInstance_table"],
                             t2 = self.database.properties["sensorValues_table"],
                             fk21 = self.database.properties["sensorValues_fk_sensorInstance"],
                             pk1 = self.database.properties["sensorInstance_pk"],
                             c3 = self.database.properties["sensorInstance_id"])
        temp = pd.read_sql_query(query, self.database.conn, params=param)
        self.sensor_data = temp.to_numpy()
        self.sensor_data = self.sensor_data.transpose()

    def query_instance_checks(self):
        """Query the different methods corresponding to checks in the database.

        Returns:
            dict: Dictionary where the keys are the checks IDs and the value are
                  the methods.
                  
        Note:
            This method only consider instance checks (mode = 1)
        """
        query = "SELECT {pk}, {c1} FROM {t} WHERE {c2} = %s"
        query = query.format(t = self.database.properties["checks_table"],
                            pk = self.database.properties["checks_pk"],
                            c1 = self.database.properties["checks_codeclass"],
                            c2 = self.database.properties["checks_mode"])
        mode_instance_check = 1 #this dictionnary is initialized only with instance checks
        param = (mode_instance_check, )
        response = pd.read_sql_query(query, self.database.conn, params=param)
        response = response.to_numpy()
        dict = {}
        for i in range(0, response.shape[0]):
            full_name = response[i][1] #full_name = "module.class.method"
            module_name = "monitoring_tool.checks." + full_name.split('.')[0] #consider import from project root
            class_name = full_name.split('.')[1]
            method_name = full_name.split('.')[2]
            try:
                module = __import__(module_name, fromlist=[None]) #imports the appropriate module, fromlist is there to ensure the module itself is imported, not a parent file
            except Exception as err:
                print(err)
                pass
            else:
                class_ = getattr(module, class_name)
                method = getattr(class_, method_name)
                dict[response[i][0]] = method
        return dict

    def get_method_from_dict(self, check_id):
        """Get the method to be run to execute the check.

        Args:
            check_id (int): ID of the check to execute.

        Returns:
            function : Method to be run to execute the check.
        """
        return self.instance_checks_dict.get(check_id)

    def display(self):
        """Display info about the sensor.
        """
        if(not self.identified):
            return
        print("Sensor ID: ", self.sensor_instance_id, " measuring data variable ", self.sensor_dimension)
        print("Location ID: ", self.sensor_location)
        print("Data: ", self.sensor_data[1,:].size, " samples (mean ", self.sensor_data[1,:].mean(), " sd ", self.sensor_data[1,:].std(), ")") 
        print("\n")
        
#====================================
#       class UserCheckConfiguration
#====================================

class UserCheckConfiguration:
    """Defines which checks are to be performed on a given sensor
    
    This class defines, for each Collector object, the checks that are to be performed.
    It is usually read in the database but can be selected manually.

    Args:
        collector (:py:class:`Collector`) : The Collector object representing
                                            the sensor.

    Attributes:
        UserCheckConfiguration.checksRequired (:obj:`ndarray` of bools) : class attribute
                                                                defining the checks
                                                                that are mandatory.
                                                                Retrieved from
                                                                the Collector's 
                                                                database.
        UserCheckConfiguration.checksNames (list of strings) : the names of the checks
        UserCheckConfiguration.nbChecks (int) : the number of checks defined in
                                                the Collector's database.
        UserCheckConfiguration.init (bool) : bool describing if the class attributes
                                            have been initialized.
        sensor_instance_id (int) : Unique ID of the Collector.
        sensor_dimension (int) : ID of the physical quantity measured by the
                                 Collector.
        sensor_location (int) : ID of the location of the collector.
        checksEnabled (:obj:`ndarray` of bools) : array of bools, describing the
                                                checks enabled for the collector.
                                                checksEnabled[idCheck] == 1 if
                                                the check of ID idCheck is enabled
                                                
    Note: 
        This class assumes that the IDs of the checks are unsigned successive integers
        starting from 1 (0 is hard-defined to be a data existence check), otherwise
        the use of arrays and lists using the check ID as key is at best non-optimal,
        or even dangerous.
    """
    
    @classmethod
    def getChecksClassAttributes(cls, db):
        """Class method to initialize the class attributes of UserCheckConfiguration.
        
        Initializes the checks available (by their name), and whether they are
        mandatory or not.
        
        Args:
            db (:py:class:`Database`) : Database in which the class attributes
                                        are found.
        
        Returns:
            req (:obj:`ndarray` of bools) : the array representing the mandatory
                                            caracteristic of every check, the
                                            index in the array being the check ID.
            names (list of strings) : the list of the names of the checks, the index
                                    in the list being the checks ID.
        """
        query = "SELECT {c1}, {c2} required FROM {t} WHERE {c3} = %s"
        query = query.format(t = db.properties["checks_table"],
                            c1 = db.properties["checks_name"],
                            c2 = db.properties["checks_req"],
                            c3 = db.properties["checks_mode"])
        mode_instance_check = 1 #this method only consider instance checks
        param = (mode_instance_check, )
        temp = pd.read_sql_query(query, db.conn, params = param)
        req = [1]
        nam = ["Data existence check"]
        req = req + temp.required.to_list()
        nam = nam + temp.name.to_list()
        req = np.array(req)
        UserCheckConfiguration.checksRequired = req
        UserCheckConfiguration.checksNames = nam
        UserCheckConfiguration.nbChecks = len(req)
        UserCheckConfiguration.init = 1
        return req, nam
    
    #class attributes
    init = 0
    checksRequired, checksNames = [None], np.zeros(1)
    nbChecks = len(checksNames)
    
    def __init__(self, collector):
        if(UserCheckConfiguration.init == 0): #the class attributes have not been initialized, ensures they are initialized after the db selection
            UserCheckConfiguration.getChecksClassAttributes(collector.database)
        self.checksEnabled = UserCheckConfiguration.checksRequired.copy()
        self.sensor_instance_id = collector.sensor_instance_id
        self.sensor_dimension = collector.sensor_dimension
        self.sensor_location = collector.sensor_location
        self.userChoiceFromDB(collector.database)
    
    def userChoiceManual(self):
        """Method to manually select UserCheckConfiguration
    
            Method to set the UserCheckConfiguration through console, for debugging
            and testing purposes only.
    
            Note:
                This method acknowledges for mandatory checks, this is not a way
                to bypass it.
        """
        print("Please type '1' to enable check, or '0' to ignore it")
        for i in range(0, UserCheckConfiguration.nbChecks):
            if(UserCheckConfiguration.checksRequired[i] == 0):
                print(UserCheckConfiguration.checksNames[i], ": ", end="")
                temp = int(input()) #debugging function only, no need to check if the input is an int
                temp = round(util.bounds(0, temp, 1))
                self.checksEnabled[i] = temp
            
    
    def userChoiceFromDB(self, database):
        """Method to read the user check configuration in a database
        
            This methods reads the database to find three configurations:
            one that suits the sensor's location, one that suits the sensor's
            measured dimension, and one that is specific to the sensor (by its 
            ID), plus the required check configuration already instantiated,
            and combines these four configurations OR-wise.
            E.g., lets imagine that check 3 is enabled for thermometers, check 2
            for sensors in the room A123, check 1 for sensor of ID 1234 and that
            the check 4 is mandatory. If the sensor 1234 is a thermometer
            located in room A123, i twill performs the checks 1, 2, 3 and 4.
    
            Args:
                database (:py:class:`Database`) : Database in which the
                                                configurations are found.
    
            Returns :
                int : ret is a three-digits binary number describing the
                        level of specificity of the configuration found. The
                        msb is set to 1 if a configuration is found for the
                        instance. The middle digit is set to 1 if a configuration
                        is found for the location. The lsb is set to 1 if a
                        configuration is found for the dimension measured by
                        the sensor. ret = 0 if no configuration is found.
        """
        db = database
        ret = 0b000
        mode_instance_check = 1 #this method only consider instance checks
        # check for sensor_instance configuration
        #first check for sensor_instance configuration
        query = ("SELECT {t1}.{c1} "
                " FROM {t1} JOIN {t2} "
                " ON {t1}.{c1} = {t2}.{pk2} "
                " WHERE {t1}.{c2} = %s "
                " AND {t2}.{c5} = %s")
        query = query.format(t1 = db.properties["checkConfiguration_table"],
                            c1 = db.properties["checkConfiguration_fk_checks"],
                            c2 = db.properties["checkConfiguration_instance"],
                            t2 = db.properties["checks_table"],
                            pk2 = db.properties["checks_pk"],
                            c5 = db.properties["checks_mode"] )
        param = (self.sensor_instance_id, mode_instance_check)
        tests_ids_instance = pd.read_sql_query(query, db.conn, params=param)
        tests_ids_instance = tests_ids_instance.transpose()
        tests_ids_instance = tests_ids_instance.to_numpy()
        tests_ids_instance = list(tests_ids_instance[0])
        # tests_ids_instance is now the list of the IDs of the tests to be run of the sensor instance
        if( len(tests_ids_instance) > 0 ): #configuration found for sensor instance
            ret += 0b100
        
        # check for sensor_location configuration
        query = ("SELECT {t1}.{c1} "
                " FROM {t1} JOIN {t2} "
                " ON {t1}.{c1} = {t2}.{pk2} "
                " WHERE {t1}.{c3} = %s "
                " AND {t2}.{c5} = %s")
        query = query.format(t1 = db.properties["checkConfiguration_table"], 
                            c1 = db.properties["checkConfiguration_fk_checks"],
                            c3 = db.properties["checkConfiguration_fk_location"],
                            t2 = db.properties["checks_table"],
                            pk2 = db.properties["checks_pk"],
                            c5 = db.properties["checks_mode"] )
        param = (self.sensor_location, mode_instance_check)
        tests_ids_location = pd.read_sql_query(query, db.conn, params=param)
        tests_ids_location = tests_ids_location.transpose()
        tests_ids_location = tests_ids_location.to_numpy()
        tests_ids_location = list(tests_ids_location[0])
        # tests_ids_location is now the list of the IDs of the tests to be run of the sensor given its location
        if( len(tests_ids_location) > 0 ): #configuration found for sensor location
            ret += 0b010
        
        # check for sensor_dimension configuration
        query = ("SELECT {t1}.{c1} "
                " FROM {t1} JOIN {t2} "
                " ON {t1}.{c1} = {t2}.{pk2} "
                " WHERE {t1}.{c4} = %s "
                " AND {t2}.{c5} = %s")
        query = query.format(t1 = db.properties["checkConfiguration_table"], 
                            c1 = db.properties["checkConfiguration_fk_checks"],
                            c4 = db.properties["checkConfiguration_dimension"],
                            t2 = db.properties["checks_table"],
                            pk2 = db.properties["checks_pk"],
                            c5 = db.properties["checks_mode"]  )
        param = (self.sensor_dimension, mode_instance_check)
        tests_ids_dimension = pd.read_sql_query(query, db.conn, params=param)
        tests_ids_dimension = tests_ids_dimension.transpose()
        tests_ids_dimension = tests_ids_dimension.to_numpy()
        tests_ids_dimension = list(tests_ids_dimension[0])
        # tests_ids_dimension is now the list of the IDs of the tests to be run of the sensor given its dimension
        if( len(tests_ids_dimension) > 0 ): #configuration found for sensor dimension
            ret += 0b001
        
        tests_ids = tests_ids_instance + tests_ids_location + tests_ids_dimension # list concatenation
        tests_ids = list( set(tests_ids) ) #remove the doubles and orders the list
        self.checksEnabled = UserCheckConfiguration.checksRequired.copy() #init: only required tests are enabled at first
        for id in tests_ids:
            id = util.bounds(0, id, UserCheckConfiguration.nbChecks-1)
            self.checksEnabled[id] = 1
        return ret
        
    def display(self):
        """Display info about the UserCheckConfiguration.
        
        For debugging purpose. The display is a list of "%i: %a (%b),", where i
        is the ID of the check, a describes if the check is enabled and b if the
        check is required.
        """
        print("User Check configuration, instance=", self.sensor_instance_id, ", dimension=", self.sensor_dimension)
        for i in range(0, self.checksEnabled.size):
            print(i,":", self.checksEnabled[i], "(", UserCheckConfiguration.checksRequired[i], "),", end=' ')
        print()
        
    
    