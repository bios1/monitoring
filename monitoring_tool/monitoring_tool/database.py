#====================================
#        Header, import modules
#====================================
import os

import mysql.connector

import pandas as pd
import numpy as np

import string
import re #regular expression

class Database:
    """Holds MySQL connection details and database properties.
    
    Args:
        filename (str): Path and filename of the database configuration.
    
    Note:
        The configuration file should contains the keys of :py:attr:`properties`
        and the corresponding value. All the keys can be found in the
        file :file:`~/.config/monitoring_tool/template.txt`. The file must be
        named <database_name>.txt. A database configuration file looks like
        this::
            database = weather

            version_sensitive = 0

            sensorDimension_table = meter_type
            sensorDimension_pk = id
            sensorDimension_id = id
            sensorDimension_name = name

            sensorInstance_table = meter
            sensorInstance_pk = id
            sensorInstance_id = meter_number
            sensorInstance_fk_sensorDimension = meter_type_id
            sensorInstance_fk_location = building_id

    Attributes:
        properties (dict): Dictionary which values are the database tables names
                           corresponding to the keys.
        conn (:obj:`MySQLConnection`): MySQL connection object used for 
                                       operation on the database.
        version (int): Version of the database.
        version_date (str): Release date of the version of the database.
    """

    def __init__(self, filename):
        self.properties = None
        self.init_properties(filename)
        self.conn = None
        self.version = None
        self.version_date = ""

    def init_properties(self, filename):
        """Initialize the :py:attr:`properties` of the database.

        Args:
            filename (str): Path and filename of the database configuration
                            (see :py:class:`Database`).
        """
        self.properties = { }
        char_allowed = string.ascii_letters + string.digits + '=' + '_' #characters allowed in code, to prevent sql injection
        try:
            path = filename
            file = open(path, mode="r")
        except FileNotFoundError:
            print("The file at", path, "has not been found")
        else:
            for line in file:
                line = line.replace('\n', '') #removes the \n at the end of lines
                code = line.split('#')[0] #separate code and comments
                re.sub('[^%s]' % char_allowed, '', code) #removes disallowed characters
                codesplit = code.split(" = ")
                if(len(codesplit) <= 2):
                    key = codesplit[0]
                    db_field_name = codesplit[-1] #works even with empty lines or full comment lines, i.e when code has only one element
                    db_field_name = db_field_name.replace(' ', '') #remove spaces from field names
                    if(key != '' and key != '\n'):
                        self.properties[key] = db_field_name


    def connect(self, config_file="{}/.config/monitoring_tool/__server_config__.txt".format(os.environ['HOME'])):
        """Connect to the database. Update :py:attr:`conn` with the 
        ``MySQLConnector`` object returned by ``mysql.connector.connect``.

        Args:
            config_file (str, optional): Configuration file of the MySQL server.
                                         The file has the following format::
                                                    hostname:port
                                                    username
                                                    password
                                         Defaults to "~/.config/monitoring_tool/__server_config__.txt".
        """
        try:
            file = open(config_file, mode='r')
        except FileNotFoundError:
            print("The file at", config_file, "has not been found")
        else:
            (hostname, port) = tuple(file.readline().split(':', 2))
            username = file.readline()
            pwd = file.readline()
            if self.properties is None:
                print("Cannot connect to database. Properties not initialized")
            else:
                db_name = self.properties["database"]
                self.conn = mysql.connector.connect(
                    host = hostname,
                    port = port,
                    database = db_name,
                    user = username,
                    password = pwd)
                
                if self.properties["version_sensitive"] == '1':
                    self.version = self.query_latest_version()
                    self.version_date = self.query_version_date(self.version)

    def disconnect(self):
        """Disconnect from the database.

        Closes the ``MySQLConnection`` object :py:attr:`conn`.
        """
        self.conn.close()

    def query_latest_version(self):
        """Query the latest version of the database in the database.

        Returns:
            int: The latest vestion of the database.
        """
        query = "SELECT MAX({c}) FROM {t}"
        query = query.format(c = self.properties["version_value"],
                            t = self.properties["version_table"])
        latest = pd.read_sql_query(query, self.conn)
        latest = latest.to_numpy() #conversion pandas.dataframe -> numpy.array
        latest = latest[0][0]
        latest = latest.item() #conversion to python type to fit in queries
        return latest

    def query_version_date(self, vers=None):
        """Query the release date of a version.

        Args:
            vers (int, optional): the version number. If set to None (default),
                                  query the release date of the latest version.

        Raises:
            ValueError: if `vers` is greater than the latest version or less 
                        than 1.

        Returns:
            str: Release date of the version.
        """
        latest_version = self.query_latest_version()
        if self.version is None:
            vers = latest_version
        if vers > latest_version or vers < 1:
            raise ValueError("Invalid version requested: requested version greter than latest version")
        query = ("SELECT {c} FROM {t}"
                 " WHERE {cond}=%s" )
        query = query.format(c = self.properties["version_timestamp"],
                            t = self.properties["version_table"],
                            cond = self.properties["version_value"])
        param = (vers, )
        date = pd.read_sql_query(query, self.conn, params=param)
        date = date.to_numpy() #conversion pandas.dataframe -> numpy.array
        date = date[0][0]
        fmt = '%Y-%m-%d %H:%M:%S'
        date = pd.to_datetime(str(date)) #reconversoin to pandas date (not dataframe) to apply format
        date = date.strftime(fmt) #conversoin to string
        return date
